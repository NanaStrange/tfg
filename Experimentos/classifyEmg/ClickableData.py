# -*- coding: utf-8 -*-
"""
Created on Tue May 10 08:47:06 2016
"""
import numpy as np
import matplotlib.pyplot as plt
from collections import deque


class ClickableData(object):
    def __init__(self, data, labels = [0, 1], picker = 5):
        self.x = []
        self.y = []
        self.labels = labels

        # standardize data and add offset in each channel for plotting purposes
        data = [(x - np.mean(x)) / np.std(x) for x in data]
        for i in range(len(data)):
            # since data is standardized, 6 is a good choice
            data[i] = data[i] + i * 6
        self.data = data
        
        self.fig = plt.figure()
        ax1 = self.fig.add_subplot(111)
        for i in range(len(self.data)):
            ax1.plot(self.data[i], "b")
        # We shall represent len(labels) horizontal lines representing the 
        # levels. First, let's compute the spacing between levels delta        
        min_val = np.min([np.min(x) for x in data]) - 0.1
        max_val = np.max([np.max(x) for x in data]) + 0.1
        delta = (max_val - min_val) / (float(len(labels)) - 1)
        self.label_levels = min_val + np.array(range(len(labels))) * delta
        for val in self.label_levels:
            ax1.axhline(val, xmin = 0, xmax = len(data[0]), color = "g",picker = picker)
        self.label_plot, = ax1.plot(self.x, self.y,"r--")
        
        self.close_cid = self.fig.canvas.mpl_connect('close_event', self.disconnect)                
        self.click_cid = self.fig.canvas.mpl_connect('button_press_event', self.onclick)
        
        #self.fig.show()
        plt.show(block = True)
        
    def onclick(self, event):
        global ix, iy
        ix, iy = event.xdata, event.ydata
        #print 'x = %d, y = %d' %(ix, iy)
    
        self.x.append(ix)
        self.y.append(iy)
        
        self.label_plot.set_xdata(self.x)
        self.label_plot.set_ydata(self.y)
        self.fig.canvas.draw()
    
    def disconnect(self, event):
        print "Disconnecting"
        self.fig.canvas.mpl_disconnect(self.click_cid)
        self.fig.canvas.mpl_disconnect(self.close_cid)  
     
    def interp0(self, x, xp, yp):
        """Zeroth order hold interpolation w/ same
        (base)   signature  as numpy.interp."""
        def func(x0, xP=deque(xp), yP=deque(yp)):
          if x0 <= xP[0]:
            return yP[0]
          if x0 >= xP[-1]:
            return yP[-1]    
          while x0 > xP[0]:
            xP.popleft()     # get rid of default
            y = yP.popleft()    # data as we go.
          return y
        return [func(i,deque(xp),deque(yp)) for i in x]
     
    def interpolateLabels(self):
        # in order ot interpolate with np.interp, x should be an increasing array
        index = np.argsort(self.x)
        new_x =  np.array(self.x)[index]
        new_y =  np.array(self.y)[index]
        # approximate y levels to the valid label_levels
        label_index = [j.argmin() for j in [abs(i - self.label_levels) for i in new_y]]
        new_y = [self.label_levels[i] for i in label_index]
        out = self.interp0(range(len(self.data[0])), new_x, new_y)
        #plt.plot(new_x,new_y,"o")
        #plt.plot(range(len(self.data[0])),out,"g")
        #plt.show()
        # get the levels for out
        return out
        
    def getLabels(self):
            label_index = [j.argmin() for j in [abs(i - self.label_levels) for i in self.interpolateLabels()]]
            return [self.labels[i] for i in label_index]
            
    def plot(self):
        nfig = plt.figure()
        ax1 = nfig.add_subplot(111)
        for i in range(len(self.data)):
            ax1.plot(self.data[i], color =  "b")
        ax1.plot(self.interpolateLabels(),"r--")
        plt.show()
        
