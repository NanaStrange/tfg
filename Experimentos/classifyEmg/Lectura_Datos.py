from xml.dom.minidom import parse
import xml.dom.minidom
import numpy as np
import pandas as pd

#Lectura del fichero de cabecera (experimentos sin acelerometro)
def lectura_informacion(filename): 

    # Abrir fichero xml
    DOMTree = xml.dom.minidom.parse(filename+".xml")
    cabecera = DOMTree.documentElement

    # Recuperar informacion
    fecha = cabecera.getElementsByTagName("fecha")[0]
    duracion = cabecera.getElementsByTagName("duracion")[0]
    config = cabecera.getElementsByTagName("config")[0]
    fchannel = cabecera.getElementsByTagName("f-channel")[0]
    faccel = cabecera.getElementsByTagName("f-accel")[0]
    formato = cabecera.getElementsByTagName("formato")[0]
    numChannels = cabecera.getElementsByTagName("num_channels")[0]
    info = fecha.childNodes[0].data + " " + duracion.childNodes[0].data + " " + config.childNodes[0].data + " " + fchannel.childNodes[0].data + " " + faccel.childNodes[0].data + " " + formato.childNodes[0].data + " " + numChannels.childNodes[0].data
    i = 0
    while i < int(numChannels.childNodes[0].data):
        name = cabecera.getElementsByTagName("name")[i]
        mode = cabecera.getElementsByTagName("mode")[i]
	info = info + " " + name.childNodes[0].data + " " + mode.childNodes[0].data
        i = i + 1
    anotaciones = cabecera.getElementsByTagName("anotaciones")[0]
    info = info + " " + anotaciones.childNodes[0].data

    # Imprimir
    #print "*****"+filename+"*****"
    #print "Fecha: %s" %fecha.childNodes[0].data
    #print "Duracion: %s" %duracion.childNodes[0].data
    #print "Configuracion: %s" %config.childNodes[0].data
    #print "Frecuencia canales: %s" %fchannel.childNodes[0].data
    #print "Frecuencia acelerador: %s" %faccel.childNodes[0].data
    #print "Formato datos: %s" %formato.childNodes[0].data
    #print "Numero de canales: %s" %numChannels.childNodes[0].data
    #i = 0
    #while i < int(numChannels.childNodes[0].data):
    #    name = cabecera.getElementsByTagName("name")[i]
    #    print "Nombre canal: %s" % name.childNodes[0].data
    #    mode = cabecera.getElementsByTagName("mode")[i]
    #    print "Channel Settings Mode: %s" % mode.childNodes[0].data
    #    i = i + 1
    #print "Anotaciones: %s" %anotaciones.childNodes[0].data

    #Devolver la estructura: "cabecera", para devolver un string de informacion: "info"
    return cabecera
     
     
#Lectura del fichero de datos (experimentos sin acelerometro)
def lectura_datos(filename): 


    ret = []
    accel = 0
    
    canal1 = []
    canal2 = []
    canal3 = []
    canal4 = []
    canal5 = []
    canal6 = []
    canal7 = []
    canal8 = []

    canalX = []
    canalY = []
    canalZ = []
    
    aux = 0
    canales = []
    
    DOMTree = xml.dom.minidom.parse(filename+".xml")
    cabecera = DOMTree.documentElement
    num_canales = cabecera.getElementsByTagName("num_channels")[0]
    i = 0
    while i < int(num_canales.childNodes[0].data):
        canales.append(cabecera.getElementsByTagName("name")[i].childNodes[0].data)
        i = i + 1    
    duracion = float(cabecera.getElementsByTagName("duracion")[0].childNodes[0].data.split(' ', 1)[0])
    
    contador = 0
    num_rows = 0
    
    for linea in open(filename+".dat",'r'):
    
    	contador = contador + 1
    	accel = accel + 1
    	if(contador == 9): 
    		contador = 1
    	if(accel == 84): 
    		accel = 1
    		contador = 1
    	
    	if(accel < 81):
    
    		if(contador == 1):
    			if str(contador) in canales:
    				canal1.append(float(linea.replace("\n", "")))
    
    		elif(contador == 2):
    			if str(contador) in canales:
    				canal2.append(float(linea.replace("\n", "")))
    
    		elif(contador == 3):
    			if str(contador) in canales:
    				canal3.append(float(linea.replace("\n", "")))
    
    		elif(contador == 4):
    			if str(contador) in canales:
    				canal4.append(float(linea.replace("\n", "")))
    
    		elif(contador == 5):
    			if str(contador) in canales:
    				canal5.append(float(linea.replace("\n", "")))
    
    		elif(contador == 6):
    			if str(contador) in canales:
    				canal6.append(float(linea.replace("\n", "")))
    
    		elif(contador == 7):
    			if str(contador) in canales:
    				canal7.append(float(linea.replace("\n", "")))
    
    		elif(contador == 8):
    			if str(contador) in canales:
    				canal8.append(float(linea.replace("\n", "")))
    	elif(accel == 81): #canal X
    		aux = linea.replace("\n", "")
    		if(aux!='undefined'):
        		canalX.append(float(aux))
    	elif(accel == 82): #canal Y
    		aux = linea.replace("\n", "")
    		if(aux!='undefined'):
        		canalY.append(float(aux))    
    	elif(accel == 83): #canal Z
    		aux = linea.replace("\n", "")
    		if(aux!='undefined'):
        		canalZ.append(float(aux))
      
    #Aseguramos que todos los canales tienen la misma longitud
    cB = []
    for channelA in canales:
        if(channelA == '1'):
            for channelB in canales:
                if(channelB == '1'):
                    cB = canal1
                elif(channelB == '2'):
                    cB = canal2
                elif(channelB == '3'):
                    cB = canal3 
                elif(channelB == '4'):
                    cB = canal4
                elif(channelB == '5'):
                    cB = canal5 
                elif(channelB == '6'):
                    cB = canal6 
                elif(channelB == '7'):
                    cB = canal7
                elif(channelB == '8'):
                    cB = canal8 
                if(len(canal1) > len(cB)):
                    canal1.pop()
                    num_rows = len(canal1)
        elif(channelA == '2'):
            for channelB in canales:
                if(channelB == '1'):
                    cB = canal1
                elif(channelB == '2'):
                    cB = canal2
                elif(channelB == '3'):
                    cB = canal3 
                elif(channelB == '4'):
                    cB = canal4
                elif(channelB == '5'):
                    cB = canal5 
                elif(channelB == '6'):
                    cB = canal6 
                elif(channelB == '7'):
                    cB = canal7
                elif(channelB == '8'):
                    cB = canal8 
                if(len(canal2) > len(cB)):
                    canal2.pop()
                    num_rows = len(canal2)
        elif(channelA == '3'):
            for channelB in canales:
                if(channelB == '1'):
                    cB = canal1
                elif(channelB == '2'):
                    cB = canal2
                elif(channelB == '3'):
                    cB = canal3 
                elif(channelB == '4'):
                    cB = canal4
                elif(channelB == '5'):
                    cB = canal5 
                elif(channelB == '6'):
                    cB = canal6 
                elif(channelB == '7'):
                    cB = canal7
                elif(channelB == '8'):
                    cB = canal8 
                if(len(canal3) > len(cB)):
                    canal3.pop()
                    num_rows = len(canal3)
        elif(channelA == '4'):
            for channelB in canales:
                if(channelB == '1'):
                    cB = canal1
                elif(channelB == '2'):
                    cB = canal2
                elif(channelB == '3'):
                    cB = canal3 
                elif(channelB == '4'):
                    cB = canal4
                elif(channelB == '5'):
                    cB = canal5 
                elif(channelB == '6'):
                    cB = canal6 
                elif(channelB == '7'):
                    cB = canal7
                elif(channelB == '8'):
                    cB = canal8 
                if(len(canal4) > len(cB)):
                    canal4.pop()
                    num_rows = len(canal4)
        elif(channelA == '5'):
            for channelB in canales:
                if(channelB == '1'):
                    cB = canal1
                elif(channelB == '2'):
                    cB = canal2
                elif(channelB == '3'):
                    cB = canal3 
                elif(channelB == '4'):
                    cB = canal4
                elif(channelB == '5'):
                    cB = canal5 
                elif(channelB == '6'):
                    cB = canal6 
                elif(channelB == '7'):
                    cB = canal7
                elif(channelB == '8'):
                    cB = canal8 
                if(len(canal5) > len(cB)):
                    canal5.pop()
                    num_rows = len(canal5)
        elif(channelA == '6'):
            for channelB in canales:
                if(channelB == '1'):
                    cB = canal1
                elif(channelB == '2'):
                    cB = canal2
                elif(channelB == '3'):
                    cB = canal3 
                elif(channelB == '4'):
                    cB = canal4
                elif(channelB == '5'):
                    cB = canal5 
                elif(channelB == '6'):
                    cB = canal6 
                elif(channelB == '7'):
                    cB = canal7
                elif(channelB == '8'):
                    cB = canal8 
                if(len(canal6) > len(cB)):
                    canal6.pop()
                    num_rows = len(canal6)
        elif(channelA == '7'):
            for channelB in canales:
                if(channelB == '1'):
                    cB = canal1
                elif(channelB == '2'):
                    cB = canal2
                elif(channelB == '3'):
                    cB = canal3 
                elif(channelB == '4'):
                    cB = canal4
                elif(channelB == '5'):
                    cB = canal5 
                elif(channelB == '6'):
                    cB = canal6 
                elif(channelB == '7'):
                    cB = canal7
                elif(channelB == '8'):
                    cB = canal8 
                if(len(canal7) > len(cB)):
                    canal7.pop()
                    num_rows = len(canal7)
                    
    pasoA = 1.0/float(25)
    pasoC = 1.0/float(250)  
    parts = filename.split('/')
    if "CA" in parts[-1]: #Si es con acelerometro    
        #Igualamos tamanos
        if(len(canalY) > len(canalZ)):
            canalY.pop()
        if(len(canalX) > len(canalY)):
            canalX.pop()
     
        i=0;
        # first accel data after 10 samples of the channel data
        time= float(10 * pasoC)
        tiempo=[];
        while(i<len(canalZ)):
            tiempo.append(time);
            time+=pasoA;
            i+=1;
    
        #Una vez aseguradas las mismas longitudes, preparamos el return
        panda_accel = pd.DataFrame({
            'tiempo': tiempo,
            'canalX': canalX,
            'canalY': canalY,
            'canalZ': canalZ
        }, columns=['tiempo', 'canalX', 'canalY', 'canalZ'])
    	
        ret.append(panda_accel)    


    i=0;
    time=0; 
    tiempo=[];
    while(i<num_rows):
            tiempo.append(time);
            time+=pasoC;
            i+=1;
            

            
    panda_chann = pd.DataFrame({
        'tiempo': tiempo
    }, columns=['tiempo'])

    
    for c in canales:
        if(c == '1'):
            panda_chann['canal 1'] = pd.Series(canal1)
        elif(c == '2'):
            panda_chann['canal 2'] = pd.Series(canal2)
        elif(c == '3'):
            panda_chann['canal 3'] = pd.Series(canal3)
        elif(c == '4'):
            panda_chann['canal 4'] = pd.Series(canal4)
        elif(c == '5'):
            panda_chann['canal 5'] = pd.Series(canal5)
        elif(c == '6'):
            panda_chann['canal 6'] = pd.Series(canal6)
        elif(c == '7'):
            panda_chann['canal 7'] = pd.Series(canal7)
        elif(c == '8'):
            panda_chann['canal 8'] = pd.Series(canal8)

    ret.append(panda_chann)
    return ret
     
#Prueba de las funciones  
#informacion = lectura_informacion("CA3S1CB01")
#print informacion
ret = lectura_datos("SA3S1CB01")
print ret