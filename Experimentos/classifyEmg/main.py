# -*- coding: utf-8 -*-
"""
Created on Tue May 10 07:21:43 2016
"""

import ClickableData
import readData
import glob
from functions import *

# path to the files to process
path = "../conAccel"
filenames = [x[:-4] for x in glob.glob(path + "/*.xml")]
# pick one file for illustratin purposes
fl = "../conAccel/S3/CA3S3EB02"
data = readData.lectura_datos(fl)

# filter the data to eliminate 50 Hz noise. 
# Data is converted to float prior to the filter call
filtered = [apply_notch50([float(x) for x in data[1]]),
           apply_notch50([float(x) for x in data[3]])]

# check the filtered data
plt.subplot(211)
#plt.plot(data[1])
plt.plot(filtered[0])

plt.subplot(212)
#plt.plot(data[3])
plt.plot(filtered[1])

# Label data
labels =  [1,2,5]
cd = ClickableData.ClickableData(filtered, picker = 100, labels = labels)
emg_labels = cd.getLabels()
cd.plot()

# Save file
f=open('CA3S3EB02.csv','w')
for x in emg_labels:
    f.write('%s \n' % x)
f.close()