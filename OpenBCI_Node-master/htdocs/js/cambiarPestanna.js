// Dadas la division que contiene todas las pestañas y la de la pestaña que se 
// quiere mostrar, la funcion oculta todas las pestañas a excepcion de esa.
var flag = 0
var prim = 0
var glob = ""

function cambiarPestanna(pestannas,pestanna, num_pest) {

    if(flag == 0) num_pest = 1;
    flag=1;

    var str =  pestanna.id;
    // Obtiene los elementos con los identificadores pasados.
    pestanna = document.getElementById(pestanna.id);
    listaPestannas = document.getElementById(pestannas.id);
    
    // Obtiene las divisiones que tienen el contenido de las pestañas.
    cpestanna = document.getElementById('c'+pestanna.id);
    listacPestannas = document.getElementById('contenido'+pestannas.id);

	//Elegir el archivo e intentar ver si el archivo es compatible
	if(num_pest == "+"){

		if (window.File && window.FileReader && window.FileList && window.Blob) {
			// Great success! All the File APIs are supported.
		} else {
			alert('El navegador es incompatible con la carga de archivos');
		}

	}

	glob = num_pest;

    //Si se ha seleccionado la pestaña +, hay que añadir una nueva pestaña y trasladar + al final de la lista
	if(num_pest == "+"){

		listaP = listaPestannas.childNodes[1].childNodes;
		PestanaMas = listaP[listaP.length-2];
		TextoPestanaMas = listaP[listaP.length-2].textContent ? listaP[listaP.length-2].textContent : listaP[listaP.length-2].innerText;

		var res = str.split("a");
		var pest = parseInt(res[2]);
		str = "pestana"+pest;

		document.getElementById("lista").removeChild(document.getElementById("lista").lastChild);
		if(prim == 0)
		document.getElementById("lista").removeChild(document.getElementById("lista").lastChild);

		prim = 1;

		var li = document.createElement("li");
		li.setAttribute("id", str);
		var a = document.createElement("a");
		a.setAttribute("href", "javascript:cambiarPestanna(pestanas,"+str+","+res[2]+");");
		a.textContent ? a.textContent = "nuevaPestanha" : a.innerText = "nuevaPestanha";
		li.appendChild(a);

		document.getElementById("lista").appendChild(li);

		pest = parseInt(res[2]) + 1;
		str = "pestana"+pest;

		var li = document.createElement("li");
		li.setAttribute("id", str);
		var a = document.createElement("a");
		a.setAttribute("href", 'javascript:cambiarPestanna(pestanas,'+str+',"+");');
		a.textContent ? a.textContent = "+" : a.innerText = "+";
		li.appendChild(a);

		document.getElementById("lista").appendChild(li);


		var div = document.createElement("div");
		div.setAttribute("id", "cpestana"+res[2]);
		var input = document.createElement("input");
		var output = document.createElement("div");
		input.setAttribute("type", "file");
		input.setAttribute("id", "files"+res[2]);
		input.setAttribute("multiple", "multiple");
		input.setAttribute("name", "files[]");
		output.setAttribute("id", "list"+res[2]);

		div.appendChild(input);
		div.appendChild(output);

		document.getElementById("contenidopestanas").appendChild(div);
		document.getElementById("files"+res[2]).addEventListener('change', cargar, false);
		glob = res[2];

	}

    i=0;
    // Recorre la lista ocultando todas las pestañas y restaurando el fondo 
    // y el padding de las pestañas.
    while (typeof listacPestannas.getElementsByTagName('div')[i] != 'undefined'){
        $(document).ready(function(){
	//Pestaña1
	    if(i != 2 && i!=1 && i!=3 && i!=4 && i!=5 && num_pest == "1"){
            	$(listacPestannas.getElementsByTagName('div')[i]).css('display','none');
	    }
	    if(( i == 2 || i==1 || i==3 || i==4 || i==5) && num_pest == "1"){
            	$(listacPestannas.getElementsByTagName('div')[i]).css('display','');
	    }
	//Pestaña2
	    if(i != 7 && num_pest == "2"){
            	$(listacPestannas.getElementsByTagName('div')[i]).css('display','none');
	    }
	    if(( i == 7) && num_pest == "2"){
            	$(listacPestannas.getElementsByTagName('div')[i]).css('display','');
	    }
	//Pestaña3
	    if(i != 9 && i != 10 && i != 11 && num_pest == "3"){
            	$(listacPestannas.getElementsByTagName('div')[i]).css('display','none');
	    }
	    if(( i == 9 || i == 10 || i == 11) && num_pest == "3"){
            	$(listacPestannas.getElementsByTagName('div')[i]).css('display','');
	    }

		if(num_pest=="+"){
			if(div.getAttribute("id") != $(listacPestannas.getElementsByTagName('div')[i].getAttribute("id"))){
				$(listacPestannas.getElementsByTagName('div')[i]).css('display', 'none');
			}
			if(div.getAttribute("id") == $(listacPestannas.getElementsByTagName('div')[i].getAttribute("id"))){
				$(listacPestannas.getElementsByTagName('div')[i]).css('display','');
			}

		}
	//Pestañas cargadas
		if(num_pest != "+" && num_pest != "1" && num_pest != "2" && num_pest != "3") {
			var factor = 12;
			var factorAux = parseInt(num_pest);
			while(factorAux!= 4){
				factor = factor + 4;
				factorAux--;
			}

			if (i!= factor && i!=factor+1 && i!=factor+2 && i!=factor+3 && parseInt(num_pest) > 3) {
				$(listacPestannas.getElementsByTagName('div')[i]).css('display', 'none');
			}
			if ((i==factor || i==factor+1 || i==factor+2 || i==factor+3 ) && parseInt(num_pest) > 3) {
				$(listacPestannas.getElementsByTagName('div')[i]).css('display', '');
			}

		}

            $(listaPestannas.getElementsByTagName('li')[i]).css('background','');
            $(listaPestannas.getElementsByTagName('li')[i]).css('padding-bottom','');
        });
        i += 1;

    }

	    if(num_pest == "3"){
		previsualizacion();
	    }
	    if(num_pest != "3"){
		no_previsualizacion();
	    }

    $(document).ready(function(){
        // Muestra el contenido de la pestaña pasada como parametro a la funcion,
        // cambia el color de la pestaña y aumenta el padding para que tape el  
        // borde superior del contenido que esta juesto debajo y se vea de este 
        // modo que esta seleccionada.
        $(cpestanna).css('display','');
        $(pestanna).css('background','dimgray');
        $(pestanna).css('padding-bottom','2px');
    });


if(num_pest == "1"){
while (document.getElementById("graficas").firstChild) {
    			document.getElementById("graficas").removeChild(document.getElementById("graficas").firstChild);
		}

		while (document.getElementById("graficas2").firstChild) {
    			document.getElementById("graficas2").removeChild(document.getElementById("graficas2").firstChild);
		}

		Timeline.prototype.sample = function (s, maximo, minimo) {
			this.data.append(new Date().getTime(), s);
		}

		var nombresCanal = new Array()
		var todos_canales=document.getElementsByName("canal");
		var num_canales=0;
		for (var i = 0; i < todos_canales.length; i++)	{
			if(todos_canales[i].checked){
				num_canales++;
				nombresCanal.push(String(i+1));
			}
		}


		var waves = [];
		for (var i = 0; i < 8; i++) { //Numero de canales 
			if(i<num_canales){
				waves.push(new Timeline(i, nombresCanal[i]));
			}
		}

		var process = 1;
		var dato = 0;
		var socket = io.connect('http://127.0.0.1');
		socket.on('openbci', function (sample) {
			var j=0;
			if (process % 3 == 0) {
				for(var i=0; i<8; i++){
					if(todos_canales[i].checked){
						dato = sample[i];
						waves[j].sample(dato);
						j++;
						if(grabando == 1){
							grabacion[grabacion.length] = dato;
						}
					}
				}	
		    		process = 1;
			}
			process++; 
		});
		socket.on('movimiento', function (mov) {
			var m = parseInt(mov.toString())
			if(m<5){ //Son estados, ningún icono debe iluminarse
				document.getElementById("ext").style.background = "rgba(0,0,0, 0.5)";
				document.getElementById("abi").style.background = "rgba(0,0,0, 0.5)";
				document.getElementById("contr").style.background = "rgba(0,0,0, 0.5)";
				document.getElementById("cerr").style.background = "rgba(0,0,0, 0.5)";
			} 
			else if(m==5){ //Extender brazo
				document.getElementById("ext").style.background = "rgba(0,255,0, 0.5)";
				document.getElementById("abi").style.background = "rgba(0,0,0, 0.5)";
				document.getElementById("contr").style.background = "rgba(0,0,0, 0.5)";
				document.getElementById("cerr").style.background = "rgba(0,0,0, 0.5)";
			}
			else if(m==6){ //Contraer brazo
				document.getElementById("ext").style.background = "rgba(0,0,0, 0.5)";
				document.getElementById("abi").style.background = "rgba(0,0,0, 0.5)";
				document.getElementById("contr").style.background = "rgba(0,255,0, 0.5)";
				document.getElementById("cerr").style.background = "rgba(0,0,0, 0.5)";
			}
			else if(m==7){ //Apretar puño 
				document.getElementById("ext").style.background = "rgba(0,0,0, 0.5)";
				document.getElementById("abi").style.background = "rgba(0,0,0, 0.5)";
				document.getElementById("contr").style.background = "rgba(0,0,0, 0.5)";
				document.getElementById("cerr").style.background = "rgba(0,255,0, 0.5)";
			}
			else if(m==8){ //Soltar puño
				document.getElementById("ext").style.background = "rgba(0,0,0, 0.5)";
				document.getElementById("abi").style.background = "rgba(0,255,0, 0.5)";
				document.getElementById("contr").style.background = "rgba(0,0,0, 0.5)";
				document.getElementById("cerr").style.background = "rgba(0,0,0, 0.5)";
			}
		});
}


if(num_pest=="+"){
	var res = pestanna.id.split("a");
	var pest = parseInt(res[2]);
	cambiarPestanna(pestannas,pestanna, pest);
}

}


function previsualizacion(){

	no_previsualizacion();

	document.getElementById("graficaPrev").style.visibility = "visible";
	var div1 = document.createElement("div");
	div1.setAttribute("id", "contenedor1");
	document.getElementById("graficaPrev").appendChild(div1);
	var div2 = document.createElement("div");
	div2.setAttribute("id", "contenedor2");
	document.getElementById("graficaPrev").appendChild(div2);
	var div3 = document.createElement("div");
	div3.setAttribute("id", "contenedor3");
	document.getElementById("graficaPrev").appendChild(div3);
	var div4 = document.createElement("div");
	div4.setAttribute("id", "contenedor4");
	document.getElementById("graficaPrev").appendChild(div4);

	document.getElementById("graficaPrev2").style.visibility = "visible";
	var div5 = document.createElement("div");
	div5.setAttribute("id", "contenedor5");
	document.getElementById("graficaPrev").appendChild(div5);
	var div6 = document.createElement("div");
	div6.setAttribute("id", "contenedor6");
	document.getElementById("graficaPrev").appendChild(div6);
	var div7 = document.createElement("div");
	div7.setAttribute("id", "contenedor7");
	document.getElementById("graficaPrev").appendChild(div7);
	var div8 = document.createElement("div");
	div8.setAttribute("id", "contenedor8");
	document.getElementById("graficaPrev").appendChild(div8);

	document.getElementById("accelPrev").style.visibility = "visible";
	var divX = document.createElement("div");
	divX.setAttribute("id", "contenedorX");
	document.getElementById("graficaPrev").appendChild(divX);
	var divY = document.createElement("div");
	divY.setAttribute("id", "contenedorY");
	document.getElementById("graficaPrev").appendChild(divY);
	var divZ = document.createElement("div");
	divZ.setAttribute("id", "contenedorZ");
	document.getElementById("graficaPrev").appendChild(divZ);


	var canal1=new Array();
	var canal2=new Array();
	var canal3=new Array();
	var canal4=new Array();
	var canal5=new Array();
	var canal6=new Array();
	var canal7=new Array();
	var canal8=new Array();

	var num_canales_act=new Array();
	num_canales_act = [];
	for (var i = 0; i < todos_canales.length; i++)	{
		if(document.getElementsByName("canal")[i].checked){
			num_canales_act.push(i+1);
		}
	}


	for (var i = 0; i < num_canales_act.length; i++){
		for(var j=0; j < grabacion.length; j++){
			if(num_canales_act[i] == 1)
				canal1.push(grabacion[num_canales_act.length*j+i]);
			if(num_canales_act[i] == 2)
				canal2.push(grabacion[num_canales_act.length*j+i]);
			if(num_canales_act[i] == 3)
				canal3.push(grabacion[num_canales_act.length*j+i]);
			if(num_canales_act[i] == 4)
				canal4.push(grabacion[num_canales_act.length*j+i]);
			if(num_canales_act[i] == 5)
				canal5.push(grabacion[num_canales_act.length*j+i]);
			if(num_canales_act[i] == 6)
				canal6.push(grabacion[num_canales_act.length*j+i]);
			if(num_canales_act[i] == 7)
				canal7.push(grabacion[num_canales_act.length*j+i]);
			if(num_canales_act[i] == 8)
				canal8.push(grabacion[num_canales_act.length*j+i]);

		}
	}

	/*
	paso = tiempo_grabacion/canal1.length;
	alert(paso);
		$('#contenedor1').highcharts({
			title: {
				text: 'Canal 1'
			},
			xAxis: {
				title: {
					text: 'tiempo (segundos)'
				},
				floor:0,
				tickInterval: 0.5,

			},
			yAxis: {
				title: {
					text: 'microVoltios (mv)'
				}
			},
			series: [{
				data: canal1
			}]
		});
*/
// Highstock
	paso = tiempo_grabacion/canal1.length;
	datos_seg = canal1.length/tiempo_grabacion;

	var aux = 0;
	var tiempoPrev = new Array();
	while(aux < tiempo_grabacion){
		tiempoPrev.push(aux.toString());
		aux += paso;
	}

	alert(tiempoPrev.length + " " + canal1.length)

	$('#contenedor1').highcharts({
		chart: {
			type: 'spline'
		},

		title: {
			text: 'Canal 1'
		},

		xAxis: {
			labels: {
				step: datos_seg
			}
		},

		yAxis: {
			title: {
				text: 'Percentage'
			}
		},

		series: [{
			data: canal1
		}]
	});


/*
	$('#contenedor1').highcharts({
		chart: {
			zoomType: 'xy'
		},
		title: {
			text: 'Canal 1'
		},
		xAxis: [{
			ceiling:tiempo_grabacion,
			floor:0,
			crosshair: true
		}],
		yAxis: [{ // Primary yAxis
			labels: {
				format: '{value}',
				style: {
					color: Highcharts.getOptions().colors[2]
				}
			},
			title: {
				text: 'Etiquetado',
				style: {
					color: Highcharts.getOptions().colors[2]
				}
			},
			opposite: true

		}, { // Secondary yAxis
		}, { // Tertiary yAxis
			gridLineWidth: 0,
			title: {
				text: 'Señal EMG',
				style: {
					color: Highcharts.getOptions().colors[1]
				}
			},
			labels: {
				format: '{value} mV',
				style: {
					color: Highcharts.getOptions().colors[1]
				}
			},

		}],
		tooltip: {
			shared: true
		},
		legend: {
			layout: 'vertical',
			align: 'left',
			x: 80,
			verticalAlign: 'top',
			y: 55,
			floating: true,
			backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
		},
		series: [ {
			name: 'Etiquetado',
			type: 'spline',
			yAxis: 2,
			data: [1016, 1016, 1015.9, 1015.5, 1012.3, 1009.5, 1009.6, 1010.2, 1013.1, 1016.9, 1018.2, 1016.7],
			marker: {
				enabled: false
			},
			dashStyle: 'shortdot',
			tooltip: {
				valueSuffix: ''
			}

		}, {
			name: 'Señal EMG',
			type: 'spline',
			data: canal1,
			tooltip: {
				valueSuffix: 'mV'
			}
		}]
	});
*/




	var tiempo = new Array();
	var auxSec = 0;
	while(auxSec <= document.getElementById("contador").value){
		if(auxSec % 1 < 0.06)
			tiempo.push(Math.trunc(auxSec));
		else tiempo.push(" ");
		auxSec=auxSec+0.0465;
	}

	var X=new Array();
	var Y=new Array();
	var Z=new Array();

	var lienzoPrev = document.createElement("canvas");
	lienzoPrev.setAttribute("id", "X");
	lienzoPrev.setAttribute("height", "200");
	lienzoPrev.setAttribute("width", "500");
	var parrafo = document.createElement("p");
	var salto = document.createElement('br');
	var contenidoParrafo = document.createTextNode("Eje X");
	parrafo.appendChild(contenidoParrafo);
	document.getElementById("accelPrev").appendChild(contenidoParrafo);
	document.getElementById("accelPrev").appendChild(lienzoPrev);
	document.getElementById("accelPrev").appendChild(salto);


	lienzoPrev = document.createElement("canvas");
	lienzoPrev.setAttribute("id", "Y");
	lienzoPrev.setAttribute("height", "200");
	lienzoPrev.setAttribute("width", "500");
	parrafo = document.createElement("p");
	contenidoParrafo = document.createTextNode("Eje Y");
	parrafo.appendChild(contenidoParrafo);
	document.getElementById("accelPrev").appendChild(contenidoParrafo);
	document.getElementById("accelPrev").appendChild(lienzoPrev);
	document.getElementById("accelPrev").appendChild(salto);

	lienzoPrev = document.createElement("canvas");
	lienzoPrev.setAttribute("id", "Z");
	lienzoPrev.setAttribute("height", "200");
	lienzoPrev.setAttribute("width", "500");
	parrafo = document.createElement("p");
	contenidoParrafo = document.createTextNode("Eje Z");
	parrafo.appendChild(contenidoParrafo);
	document.getElementById("accelPrev").appendChild(contenidoParrafo);
	document.getElementById("accelPrev").appendChild(lienzoPrev);


	for (var i = 0; i < 3; i++){
		for(var j=0; j < grabacionAccel.length; j++){
			if(i==0 || i%3==0)
				X.push(grabacionAccel[3*j+i]);
			if(i==1 || i%3==1)
				Y.push(grabacionAccel[3*j+i]);
			if(i==2 || i%3==2)
				Z.push(grabacionAccel[3*j+i]);

		}
	}

	var datosX = {
		labels: tiempo,
		datasets: [
			{
				label: "Eje X",
				fillColor: "rgba(255,255,255,0.2)",
				strokeColor: "rgba(255,187,205,1)",
				pointColor: "rgba(255,187,205,1)",
				pointStrokeColor: "#fff",
				data: X
			}
		]
	};

	var datosY = {
		labels: tiempo,
		datasets: [
			{
				label: "Eje Y",
				fillColor: "rgba(255,255,255,0.2)",
				strokeColor: "rgba(255,187,205,1)",
				pointColor: "rgba(255,187,205,1)",
				pointStrokeColor: "#fff",
				data: Y
			}
		]
	};

	var datosZ = {
		labels: tiempo,
		datasets: [
			{
				label: "Eje Z",
				fillColor: "rgba(255,255,255,0.2)",
				strokeColor: "rgba(255,187,205,1)",
				pointColor: "rgba(255,187,205,1)",
				pointStrokeColor: "#fff",
				data: Z
			}
		]
	};

	var myLineChart1 = new Chart(document.getElementById("X").getContext("2d")).Line(datosX, {scaleFontSize : 13, scaleFontColor : "#ffa45e" });
	var myLineChart2 = new Chart(document.getElementById("Y").getContext("2d")).Line(datosY, {scaleFontSize : 13, scaleFontColor : "#ffa45e" });
	var myLineChart3 = new Chart(document.getElementById("Z").getContext("2d")).Line(datosZ, {scaleFontSize : 13, scaleFontColor : "#ffa45e" });

}

function no_previsualizacion(){
	if ( document.getElementById("accelPrev").hasChildNodes() ){
		while ( document.getElementById("accelPrev").childNodes.length >= 1 ){
			document.getElementById("accelPrev").removeChild(document.getElementById("accelPrev").firstChild);
		}
	}

	if ( document.getElementById("graficaPrev").hasChildNodes() ){
		while ( document.getElementById("graficaPrev").childNodes.length >= 1 ){
			document.getElementById("graficaPrev").removeChild(document.getElementById("graficaPrev").firstChild);
		}
	}
	if ( document.getElementById("graficaPrev2").hasChildNodes() ){
		while ( document.getElementById("graficaPrev2").childNodes.length >= 1 ){
			document.getElementById("graficaPrev2").removeChild(document.getElementById("graficaPrev2").firstChild);
		}
	}

}


function cargar(evt) {
	var files = evt.target.files; // FileList object

	var num_canaleC = 0;
	var canalesC = [];
	var time = 0;

	var nombre = "";
	if (files.length != 3) alert("Seleccionar un fichero .xml, otro .dat y otro .csv.");
	else {

		var nombre1 = files[0].name;
		var nombre2 = files[1].name;
		var nombre3 = files[2].name;
		var nom1 = nombre1.split(".");
		var nom2 = nombre2.split(".");
		var nom3 = nombre3.split(".");

		if ((nom1[1] == "xml" && nom2[1] == "dat" && nom3[1] == "csv") || (nom1[1] == "dat" && nom2[1] == "xml" && nom3[1] == "csv") || (nom1[1] == "csv" && nom2[1] == "xml" && nom3[1] == "dat") || (nom1[1] == "dat" && nom2[1] == "csv" && nom3[1] == "xml") || (nom1[1] == "csv" && nom2[1] == "dat" && nom3[1] == "xml") || (nom1[1] == "xml" && nom2[1] == "csv" && nom3[1] == "dat")) {
			if (nom1[0] != nom2[0] || nom1[0] != nom3[0]) alert("Seleccionar tres ficheros con el mismo nombre.");
			else {

				for (var i = 0, f; f = files[i]; i++) {
					nombre = f.name;
					var nom = nombre.split(".");
					var cabecera = document.createElement("p");
					if(nom[1] == "xml"){

						var reader = new FileReader();

						reader.onload = function (e) {

							var p1 = e.target.result.split('>');
							//p.textContent = e.target.result;

							cabecera.textContent = "Fecha: " + p1[3].split('<')[0]+". Tiempo: "+p1[5].split('<')[0]+". Configuración: "+p1[7].split('<')[0]
								+". Frecuencia canal: "+p1[10].split('<')[0]+". Frecuencia acelerómetro: "+p1[12].split('<')[0]+". Unidades canal: "+p1[16].split('<')[0]
								+". Unidades acelerómetro: "+p1[18].split('<')[0]+". Formato: "+p1[21].split('<')[0]+". Numero de canales: "+p1[23].split('<')[0]
								+". Channel Settings Mode: canal "+p1[26].split('<')[0]+": "+p1[28].split('<')[0] + ", canal "+p1[30].split('<')[0]+": "+p1[32].split('<')[0]
							    +".\n Anotaciones: " + p1[35].split('<')[0];
							time = p1[5].split('<')[0].split(' ')[0];
							var doc = e.target.result;
							var frags = doc.split("\n"); var j=0; var canalesArchivo=0;
							while(j<frags.length){

								if(j==14){
									var c = frags[j].split(">");
									var ca = c[1].split("<");
									num_canaleC = parseInt(ca[0]);
								}

								if(canalesArchivo<num_canaleC && j>16 && j%2!=0){
									canalesArchivo++;
									var can = frags[j].split(">");
									var cana = can[1].split("<");
									canalesC.push(parseInt(cana[0]));
								}

								j++;
							}
						};
						reader.readAsText(f);
					}

					if(nom[1] == "dat"){

						var table = document.createElement("table");
						var tr = document.createElement("tr");
						var td1 = document.createElement("td");
						var td2 = document.createElement("td");
						var td3 = document.createElement("td");
						td2.setAttribute("id","td2"+glob);
						td3.setAttribute("id","td3"+glob);
						td1.setAttribute("width", "1100");

						var readerD = new FileReader();
						var nomAcc = nombre.split(".");
						var acc=nomAcc[0].split("A");
						var p = document.createElement("p");
						var etiquetas = document.createElement("p");
						readerD.onload = function (d) {

							var k = 0;
							while(k<canalesC.length){
								
								var tiempo = [];
								var tiempoA = [];
								var div = document.createElement("div");
								div.setAttribute("id",glob+"canal"+canalesC[k]+k);
								div.setAttribute("style","display: blocked;");
								document.getElementById('list' + glob).appendChild(div);
								td1.appendChild(div);

								var aux = parseInt(canalesC[k]);
								var canal = [];
								var canalX = [];
								var canalY = [];
								var canalZ = [];
								var numeros = d.target.result.split("\n");
								var h=0; var flagAccel = 0; nums_chan = 0; nums_accel = 0;
				
								while(h<numeros.length){
									if(h == aux-1 && flagAccel<80) {
										canal.push(numeros[h]);
										aux = aux + 8;
										nums_chan = nums_chan + 1;
									}
									if(flagAccel>79) { //Visualizar solo si nombre archivo empieza por CA							
										if(acc[0]=="C"){
											if(flagAccel==80 && numeros[h] != 'undefined') {canalX.push(numeros[h]); nums_accel = nums_accel + 1;}
											if(flagAccel==81 && numeros[h] != 'undefined') {canalY.push(numeros[h]); nums_accel = nums_accel + 1;}
											if(flagAccel==82 && numeros[h] != 'undefined') {canalZ.push(numeros[h]); nums_accel = nums_accel + 1;}
										}
										aux++;
									}

									h++;
									flagAccel++;
									if(flagAccel == 83) flagAccel=0;
								}


								//Igualamos el numero de etiquetas al numero de datos del canal
								var datos_etiquetado_aux = []
								var contador = 0, i = 0;
								while(datos_etiquetado_aux.length<canal.length){
									datos_etiquetado_aux.push(datos_etiquetado[i]);
									i += 1;
								}

								var datosXsec = canal.length/parseInt(time);
								var datosPasados = 0;
								var sec = 0;
								tiempo.push(0);
								while(tiempo.length<canal.length/2) { /***/
									if (datosPasados < datosXsec){
										datosPasados += 1;
										tiempo.push("");
									}
									else {
										sec = sec + 1;
										tiempo.push(sec);
										datosPasados = 0;
									}
								}


var u=0, canalAux = [];
while(u<canal.length){
	var num= parseFloat(canal[u]);
	canalAux.push(num);
	u= u+1;
}


$('#'+glob+"canal"+canalesC[k]+k).highcharts('StockChart', {

            title: {
                text: 'Canal '+canalesC[k]
            },
	    xAxis: [{tickInterval: 1000}],
            yAxis: [{
                labels: {
                		align: 'left'                 
                },
                title: {
                    text: 'mV'
                }
            }, {
                labels: {
                    align: 'right'
                },
                title: {
                    text: 'etiqueta'
                }
            }],

            series: [{
                type: 'line',
                name: 'mV',
                data: canalAux,
                pointStart: 0,
		yAxis: 0,
            		pointInterval: datosXsec*2,
                dataGrouping: [{dateTimeLabelFormats: 'second'}]
            }, {
                type: 'line',
                name: 'etiqueta',
                data: datos_etiquetado_aux,
                yAxis: 1,
                pointStart: 0,
            		pointInterval: datosXsec*2,
                dataGrouping: [{dateTimeLabelFormats: 'second'}]
            }]
        });


								k++;

							}
							if(acc[0]=="C"){
								var timeAux = (nums_accel / 3) / time;
								var timeA = parseInt(timeAux);
								var aux = 0; tiempoA.push("0"); var secs=1; var aux2 = 1;
								while(aux < nums_accel / 3){
									if(aux2 < timeA){
										tiempoA.push("");
										aux2 = aux2 + 1;
									}else{
										tiempoA.push(secs);
										aux2 = 0;
										secs = secs + 1;
									}
									aux= aux + 1;
								}
								cargarAccel(canalX,canalY,canalZ, tiempoA, datos_etiquetado);
							}
						};

						tr.appendChild(td1);
						tr.appendChild(td2);
						tr.appendChild(td3);
						table.appendChild(tr);

						readerD.readAsText(f);
					}

					if(nom[1]=="csv"){
						var reader = new FileReader();
						var datos_etiquetado;

						reader.onload = function (c) {
							datos_etiquetado = c.target.result.split("\n");
						};
						reader.readAsText(f);
					}


				}

				document.getElementById('list' + glob).appendChild(cabecera);
				document.getElementById("list"+glob).appendChild(table);

				var nom = nombre.split(".");
				
				document.getElementById("files" + glob).setAttribute("style","display: none;");
				document.getElementById("pestana" + glob).firstChild.textContent ? document.getElementById("pestana" + glob).firstChild.textContent = nom[0] : document.getElementById("pestana" + glob).firstChild.innerText = nom[0];

			}
		}
		else alert("Seleccionar un fichero .xml, otro .dat y otro .csv.");
	}
}


function cargarAccel(canalX,canalY,canalZ, tiempoA, datos_etiquetado){

	var datos_etiquetado_aux = []

	//Igualamos el numero de etiquetas al numero de datos del acelerometro
	var contador = 0, i = 0;
	while(datos_etiquetado_aux.length!=canalX.length){
		if(contador < 10) {
			contador += 1;
		}else{
			datos_etiquetado_aux.push(datos_etiquetado[i]);
			contador = 0;
		}
		i += 1;

	}

	var datosX = [];
	datosX = {
		labels: tiempoA,
		datasets_Y1: [
			{
				fillColor: "rgba(255,255,255,0.2)",
				strokeColor: "rgba(255,187,205,1)",
				pointColor: "rgba(255,187,205,1)",
				pointStrokeColor: "#fff",
				data: canalX
			}
		],
		datasets_Y2 : [
			{
				fillColor : "rgba(0,187,0,0)",
				strokeColor : "rgba(0,187,0,0.5)",
				pointColor : "rgba(0,187,0,1)",
				pointStrokeColor : "#fff",
				data : datos_etiquetado_aux
			}
		]
	};

	var datosY = [];
	datosY = {
		labels: tiempoA,
		datasets_Y1: [
			{
				fillColor: "rgba(255,255,255,0.2)",
				strokeColor: "rgba(255,187,205,1)",
				pointColor: "rgba(255,187,205,1)",
				pointStrokeColor: "#fff",
				data: canalY
			}
		],
		datasets_Y2 : [
			{
				fillColor : "rgba(0,187,0,0)",
				strokeColor : "rgba(0,187,0,0.5)",
				pointColor : "rgba(0,187,0,1)",
				pointStrokeColor : "#fff",
				data : datos_etiquetado_aux
			}
		]
	};

	var datosZ = [];
	datosZ = {
		labels: tiempoA,
		datasets_Y1: [
			{
				fillColor: "rgba(255,255,255,0.2)",
				strokeColor: "rgba(255,187,205,1)",
				pointColor: "rgba(255,187,205,1)",
				pointStrokeColor: "#fff",
				data: canalZ
			}
		],
		datasets_Y2 : [
			{
				fillColor : "rgba(0,187,0,0)",
				strokeColor : "rgba(0,187,0,0.5)",
				pointColor : "rgba(0,187,0,1)",
				pointStrokeColor : "#fff",
				data : datos_etiquetado_aux
			}
		]
	};


	var span = document.createElement("span");
	document.getElementById("td2"+glob).appendChild(span);

	var parrafoX = document.createElement("p");
	var parrafoY = document.createElement("p");
	var parrafoZ = document.createElement("p");
	var salto = document.createElement('br');
	var contenidoParrafoX = document.createTextNode("Canal X");
	var contenidoParrafoY = document.createTextNode("Canal Y");
	var contenidoParrafoZ = document.createTextNode("Canal Z");
	parrafoX.appendChild(contenidoParrafoX);
	parrafoY.appendChild(contenidoParrafoY);
	parrafoZ.appendChild(contenidoParrafoZ);

	var lienzoPrevX = document.createElement("canvas");
	lienzoPrevX.setAttribute("id", "canvasX"+glob);
	lienzoPrevX.setAttribute("height", "200");
	lienzoPrevX.setAttribute("width", "630");

	var lienzoPrevY = document.createElement("canvas");
	lienzoPrevY.setAttribute("id", "canvasY"+glob);
	lienzoPrevY.setAttribute("height", "200");
	lienzoPrevY.setAttribute("width", "630");

	var lienzoPrevZ = document.createElement("canvas");
	lienzoPrevZ.setAttribute("id", "canvasZ"+glob);
	lienzoPrevZ.setAttribute("height", "200");
	lienzoPrevZ.setAttribute("width", "630");

	span.appendChild(parrafoX);
	span.appendChild(lienzoPrevX);
	span.appendChild(salto);

	span.appendChild(parrafoY);
	span.appendChild(lienzoPrevY);
	span.appendChild(salto);

	span.appendChild(parrafoZ);
	span.appendChild(lienzoPrevZ);
	span.appendChild(salto);

	var myLineChart = new Chart(document.getElementById("canvasX"+glob).getContext("2d")).LineDoubleY(datosX, {scaleFontSize : 13, scaleFontColor : "#ffa45e" , Y2_scaleStartValue:0, Y2_scaleStepWidth:1});
	var myLineChart = new Chart(document.getElementById("canvasY"+glob).getContext("2d")).LineDoubleY(datosY, {scaleFontSize : 13, scaleFontColor : "#ffa45e" , Y2_scaleStartValue:0, Y2_scaleStepWidth:1});
	var myLineChart = new Chart(document.getElementById("canvasZ"+glob).getContext("2d")).LineDoubleY(datosZ, {scaleFontSize : 13, scaleFontColor : "#ffa45e" , Y2_scaleStartValue:0, Y2_scaleStepWidth:1});

}
