var pest=1;

function funcion(){
	document.getElementById('file-1').addEventListener('change', cargar, false);
	document.getElementById('file-2').addEventListener('change', cargar, false);
	document.getElementById('file-3').addEventListener('change', cargar, false);
	document.getElementById('file-4').addEventListener('change', cargar, false);
	document.getElementById('file-5').addEventListener('change', cargar, false);
	document.getElementById('content-2').style.visibility = 'hidden';
	document.getElementById('content-3').style.visibility = 'hidden';
	document.getElementById('content-4').style.visibility = 'hidden';
	document.getElementById('content-5').style.visibility = 'hidden';
	document.getElementById('content-1').style.zIndex = "0";
	document.getElementById('content-2').style.zIndex = "-100";
	document.getElementById('content-3').style.zIndex = "-100";
	document.getElementById('content-4').style.zIndex = "-100";
	document.getElementById('content-5').style.zIndex = "-100";
	
}

function pest_actual(num){
	pest=num;
	if(pest==1){
		document.getElementById('content-1').style.visibility = 'visible';
		document.getElementById('content-2').style.visibility = 'hidden';
		document.getElementById('content-3').style.visibility = 'hidden';
		document.getElementById('content-4').style.visibility = 'hidden';
		document.getElementById('content-5').style.visibility = 'hidden';
		document.getElementById('content-1').style.zIndex= 0;
		document.getElementById('content-2').style.zIndex = -100;
		document.getElementById('content-3').style.zIndex = -100;
		document.getElementById('content-4').style.zIndex = -100;
		document.getElementById('content-5').style.zIndex = -100;
	}
	else if(pest==2){
		document.getElementById('content-2').style.visibility = 'visible';
		document.getElementById('content-1').style.visibility = 'hidden';
		document.getElementById('content-3').style.visibility = 'hidden';
		document.getElementById('content-4').style.visibility = 'hidden';
		document.getElementById('content-5').style.visibility = 'hidden';
		document.getElementById('content-2').style.zIndex = 0;
		document.getElementById('content-1').style.zIndex = -100;
		document.getElementById('content-3').style.zIndex = -100;
		document.getElementById('content-4').style.zIndex = -100;
		document.getElementById('content-5').style.zIndex = -100;
	}
	else if(pest==3){
		document.getElementById('content-3').style.visibility = 'visible';
		document.getElementById('content-1').style.visibility = 'hidden';
		document.getElementById('content-2').style.visibility = 'hidden';
		document.getElementById('content-4').style.visibility = 'hidden';
		document.getElementById('content-5').style.visibility = 'hidden';
		document.getElementById('content-3').style.zIndex = 0;
		document.getElementById('content-2').style.zIndex = -100;
		document.getElementById('content-1').style.zIndex = -100;
		document.getElementById('content-4').style.zIndex = -100;
		document.getElementById('content-5').style.zIndex= -100;
	}
	else if(pest==4){
		document.getElementById('content-4').style.visibility = 'visible';
		document.getElementById('content-1').style.visibility = 'hidden';
		document.getElementById('content-3').style.visibility = 'hidden';
		document.getElementById('content-2').style.visibility = 'hidden';
		document.getElementById('content-5').style.visibility = 'hidden';
		document.getElementById('content-4').style.zIndex = 0;
		document.getElementById('content-2').style.zIndex = -100;
		document.getElementById('content-3').style.zIndex = -100;
		document.getElementById('content-1').style.zIndex = -100;
		document.getElementById('content-5').style.zIndex = -100;
	}
	else if(pest==5){
		document.getElementById('content-5').style.visibility = 'visible';
		document.getElementById('content-1').style.visibility = 'hidden';
		document.getElementById('content-3').style.visibility = 'hidden';
		document.getElementById('content-4').style.visibility = 'hidden';
		document.getElementById('content-2').style.visibility = 'hidden';
		document.getElementById('content-5').style.zIndex = 0;
		document.getElementById('content-2').style.zIndex = -100;
		document.getElementById('content-3').style.zIndex = -100;
		document.getElementById('content-4').style.zIndex= -100;
		document.getElementById('content-1').style.zIndex = -100;
	}
}

var canalesC = [];
function cargar(evt) {
	var files = evt.target.files;

	var num_canaleC = 0;

	var time = 0;
	var tiempo_grabacion = 0;
	var done = 0;

	var nombre = "";
	if (files.length != 3) alert("Seleccionar un fichero .xml, otro .dat y otro .csv.");
	else {

		var nombre1 = files[0].name;
		var nombre2 = files[1].name;
		var nombre3 = files[2].name;
		var nom1 = nombre1.split(".");
		var nom2 = nombre2.split(".");
		var nom3 = nombre3.split(".");

		for (var i = 0; i < files.length; i++) {

			file = files[i];
			alert(file.name);
		}

		if ((nom1[1] == "xml" && nom2[1] == "dat" && nom3[1] == "csv") || (nom1[1] == "dat" && nom2[1] == "xml" && nom3[1] == "csv") || (nom1[1] == "csv" && nom2[1] == "xml" && nom3[1] == "dat") || (nom1[1] == "dat" && nom2[1] == "csv" && nom3[1] == "xml") || (nom1[1] == "csv" && nom2[1] == "dat" && nom3[1] == "xml") || (nom1[1] == "xml" && nom2[1] == "csv" && nom3[1] == "dat")) {
			if (nom1[0] != nom2[0] || nom1[0] != nom3[0]) alert("Seleccionar tres ficheros con el mismo nombre.");
			else {
				//Lo primero es eliminar lo que hubiera antes
				//Cargamos los ficheros
				for (var i = 0, f; f = files[i]; i++) {
					nombre = f.name;
					var nom = nombre.split(".");
					if(nom[1] == "xml"){

						var reader = new FileReader();
						reader.onload = function (e) {
							//Contenido cabecera:
							var p1 = e.target.result.split('>');
							var element = document.getElementById("file-"+pest);
							element.parentNode.removeChild(element);

							var nombre = document.createElement("p");
							nombre.textContent = 'NOMBRE: '+nom1[0];
							document.getElementById("izq"+pest).appendChild(nombre);
							document.getElementById("tab-label-"+pest).innerHTML = nom1[0];

							var fecha = document.createElement("p");
							fecha.textContent = "FECHA: "+p1[3].split('<')[0];
							document.getElementById("izq"+pest).appendChild(fecha);

							var tiempo = document.createElement("p");
							tiempo.textContent = "DURACIÓN: "+p1[5].split('<')[0];
							document.getElementById("izq"+pest).appendChild(tiempo);

							var conf = document.createElement("p");
							conf.textContent = "CONFIGURACIÓN: "+p1[7].split('<')[0];
							document.getElementById("izq"+pest).appendChild(fecha);

							var freq = document.createElement("p");
							freq.textContent = "FRECUENCIA CANALES: "+p1[10].split('<')[0];
							document.getElementById("izq"+pest).appendChild(freq);

							var freqA = document.createElement("p");
							freqA.textContent = "FRECUENCIA ACELERÓMETRO: "+p1[12].split('<')[0];
							document.getElementById("izq"+pest).appendChild(freqA);

							var ud = document.createElement("p");
							ud.textContent = "UNIDADES CANALES: "+p1[16].split('<')[0];
							document.getElementById("izq"+pest).appendChild(ud);

							var udA = document.createElement("p");
							udA.textContent = "UNIDADES ACELERÓMETRO: "+p1[18].split('<')[0];
							document.getElementById("izq"+pest).appendChild(udA);

							var formato = document.createElement("p");
							formato.textContent = "FORMATO DE DATOS: "+p1[21].split('<')[0];
							document.getElementById("izq"+pest).appendChild(formato);

							var can = document.createElement("p");
							can.textContent = "NÚMERO DE CANALES: "+p1[23].split('<')[0];
							document.getElementById("izq"+pest).appendChild(can);

							var k = 0; var aux = 24;
							while(k<parseInt(p1[23].split('<')[0])){
								var chan = document.createElement("p");
								aux += 2;
								chan.textContent = " > CANAL "+p1[aux].split('<')[0]+ " AJUSTES: " +p1[aux+2].split('<')[0];
								document.getElementById("izq"+pest).appendChild(chan);
								aux += 2;
								k+=1;
							}

							var ano = document.createElement("p");
							ano.textContent = "ANOTACIONES: "+p1[aux+3].split('<')[0];
							document.getElementById("izq"+pest).appendChild(ano);

							tiempo_grabacion = parseFloat(p1[5].split('<')[0]);

							time = p1[5].split('<')[0].split(' ')[0];

							//Array canales:
							var doc = e.target.result;
							var frags = doc.split("\n"); var j=0; var canalesArchivo=0;
							while(j<frags.length){

								if(j==14){
									var c = frags[j].split(">");
									var ca = c[1].split("<");
									num_canaleC = parseInt(ca[0]);
								}

								if(canalesArchivo<num_canaleC && j>16 && j%2!=0){
									canalesArchivo++;
									var can = frags[j].split(">");
									var cana = can[1].split("<");
									canalesC.push(parseInt(cana[0]));
								}

								j++;
							}
						};
						reader.readAsText(f);
					}
				}

				for (var i = 0, f; f = files[i]; i++) {
					nombre = f.name;
					var nom = nombre.split(".");

					if(nom[1]=="csv"){
						var reader = new FileReader();
						var datos_etiquetado;
						reader.onload = function (c) {
							datos_etiquetado = c.target.result.split("\n");
						};
						reader.readAsText(f);
					}
				}

				for (var i = 0, f; f = files[i]; i++) {
					nombre = f.name;
					var nom = nombre.split(".");
					if(nom[1] == "dat"){

						var table = document.createElement("table");
						table.setAttribute("width", "90%");
						document.getElementById("der"+pest).appendChild(table);
						var tr = document.createElement("tr");
						var tr2 = document.createElement("tr");
						var td1 = document.createElement("td");
						var td2 = document.createElement("td");
						var td3 = document.createElement("td");
						td1.setAttribute("valign","top");
						td2.setAttribute("valign","top");
						td3.setAttribute("valign","top");
						td3.setAttribute("id","td3-"+pest);
						td3.setAttribute("colspan", "2");
						td2.setAttribute("id","td2-"+pest);
						td1.setAttribute("id","td1-"+pest);

						var readerD = new FileReader();
						var acc=nom[0].split("A");

						if(acc[0]=="C"){
							var divX = document.createElement("div");
							divX.setAttribute("id","canalX-"+pest);
							var divY = document.createElement("div");
							divY.setAttribute("id","canalY-"+pest);
							var divZ = document.createElement("div");
							divZ.setAttribute("id","canalZ-"+pest);
							td3.appendChild(divX);
							td3.appendChild(divY);
							td3.appendChild(divZ);
						}

						readerD.onload = function (d) {

							var k = 0;
							while(k<canalesC.length){

								var tiempo = [];
								var tiempoA = [];
								var div = document.createElement("div");
								div.setAttribute("id","canal"+canalesC[k]+pest);
								div.setAttribute("style", "display:block;");
								if(k<4){
									td1.appendChild(div);
								}else{
									td2.appendChild(div);
								}

								var aux = parseInt(canalesC[k]);
								var canal = [];
								var canalX = [];
								var canalY = [];
								var canalZ = [];
								var numeros = d.target.result.split("\n");
								var h=0; var flagAccel = 0; nums_chan = 0; nums_accel = 0;

								while(h<numeros.length){
									if(h == aux-1 && flagAccel<80) { //Guardamos los datos del canal k
										canal.push(numeros[h]);
										aux = aux + 8;
										nums_chan = nums_chan + 1;
									}
									if(flagAccel>79) { //Si hay acelerómetro
										if(acc[0]=="C" && done == 0){ //Guardamos los datos de los 3
											if(flagAccel==80 && numeros[h] != 'undefined') {canalX.push(numeros[h]); nums_accel = nums_accel + 1;}
											if(flagAccel==81 && numeros[h] != 'undefined') {canalY.push(numeros[h]); nums_accel = nums_accel + 1;}
											if(flagAccel==82 && numeros[h] != 'undefined') {canalZ.push(numeros[h]); nums_accel = nums_accel + 1;}
										}
										aux++;
									}

									h++;
									flagAccel++;
									if(flagAccel == 83) flagAccel=0;
								}


								//Igualamos el numero de etiquetas al numero de datos del canal
								var datos_etiquetado_aux = []
								var contador = 0, i = 0;
								while(datos_etiquetado_aux.length<canal.length){
									datos_etiquetado_aux.push(parseInt(datos_etiquetado[i]));
									i += 1;
								}


								var datosXsec = canal.length/parseInt(time);
								var datosPasados = 0;
								var sec = 0;
								tiempo.push(0);
								while(tiempo.length<canal.length/2){
									if (datosPasados < datosXsec){
										datosPasados += 1;
										tiempo.push("");
									}
									else {
										sec = sec + 1;
										tiempo.push(sec);
										datosPasados = 0;
									}
								}

								var u=0, canalAux = [];
								while(u<canal.length){
									var num= parseFloat(canal[u]);
									canalAux.push(num);
									u= u+1;
								}

								$("#canal"+canalesC[k]+pest).highcharts('StockChart', {
									rangeSelector: {
										selected: 4,
										inputEnabled: false,
										buttonTheme: {
											visibility: 'hidden'
										},
										labelStyle: {
											visibility: 'hidden'
										}
									},
									chart: {
										height: 200,
										width: 423,
										type: 'line'
									},
									title: {
										text: 'Canal '+canalesC[k]
									},
									xAxis: [{tickInterval: 1000}],
									yAxis: [{
										labels: {
											align: 'left'
										},
										title: {
											text: 'mV'
										}
									}, {
										labels: {
											align: 'right'
										},
										title: {
											text: 'etiqueta'
										}
									}],

									series: [{
										type: 'line',
										name: 'mV',
										data: canalAux,
										pointStart: 0,
										yAxis: 0,
										pointInterval: datosXsec*2,
										dataGrouping: [{dateTimeLabelFormats: 'second'}]
									}, {
										type: 'line',
										name: 'etiqueta',
										data: datos_etiquetado_aux,
										yAxis: 1,
										pointStart: 0,
										pointInterval: datosXsec*2,
										dataGrouping: [{dateTimeLabelFormats: 'second'}]
									}]
								});


								k++;

							}/*
							 if(acc[0]=="C" && done==0){
							 done = 1;
							 var timeAux = (nums_accel / 3) / time;
							 var timeA = parseInt(timeAux);
							 var aux = 0; tiempoA.push("0"); var secs=1; var aux2 = 1;
							 while(aux < nums_accel / 3){
							 if(aux2 < timeA){
							 tiempoA.push("");
							 aux2 = aux2 + 1;
							 }else{
							 tiempoA.push(secs);
							 aux2 = 0;
							 secs = secs + 1;
							 }
							 aux= aux + 1;
							 }
							 cargarAccel(canalX,canalY,canalZ, tiempo_grabacion, datos_etiquetado);
							 } */
						};
						var k=0;
						while(k<2){
							if(k<4){
								td1.appendChild(document.getElementById("canal"+2+pest));
							}else{
								td2.appendChild(document.getElementById("canal"+canalesC[k]+pest));
							}
							k+=1;
						}
						tr.appendChild(td1);
						tr.appendChild(td2);
						tr2.appendChild(td3);
						table.appendChild(tr);
						table.appendChild(tr2);

						readerD.readAsText(f);
					}
				}
			}
		}
		else alert("Seleccionar un fichero .xml, otro .dat y otro .csv.");
	}
}
/*

function cargarAccel(canalX,canalY,canalZ, duracion, datos_etiquetado){

	var datos_etiquetado_aux = []

	//Igualamos el numero de etiquetas al numero de datos del acelerometro
	var contador = 0, i = 0;
	while(datos_etiquetado_aux.length!=canalX.length){
		if(contador < 10) {
			contador += 1;
		}else{
			datos_etiquetado_aux.push(parseInt(datos_etiquetado[i]));
			contador = 0;
		}
		i += 1;

	}

	var paso = duracion/canalZ.length;


	var u=0, canalX2 = [];
	while(u<canalX.length){
		var num= parseFloat(canalX[u]);
		canalX2.push(num);
		u= u+1;
	}
	u=0; var canalY2 = [];
	while(u<canalY.length){
		var num= parseFloat(canalY[u]);
		canalY2.push(num);
		u= u+1;
	}
	u=0; var canalZ2 = [];
	while(u<canalZ.length){
		var num= parseFloat(canalZ[u]);
		canalZ2.push(num);
		u= u+1;
	}

	$('#'+glob+"canalX").highcharts('StockChart', {
		rangeSelector: {
			selected: 4,
			inputEnabled: false,
			buttonTheme: {
				visibility: 'hidden'
			},
			labelStyle: {
				visibility: 'hidden'
			}
		},
		chart: {
			height: 250,
			width: 600,
			type: 'line'
		},
		navigator: {
			enabled: false
		},
		title: {
			text: 'Canal X'
		},
		xAxis: [{tickInterval: 1000}],
		yAxis: [{
			labels: {
				align: 'left'
			},
			title: {
				text: 'G'
			}
		}, {
			labels: {
				align: 'right'
			},
			title: {
				text: 'etiqueta'
			}
		}],

		series: [{
			type: 'line',
			name: 'mV',
			data: canalX2,
			pointStart: 0,
			yAxis: 0,
			pointInterval: paso,
			dataGrouping: [{dateTimeLabelFormats: 'second'}]
		}, {
			type: 'line',
			name: 'etiqueta',
			data: datos_etiquetado_aux,
			yAxis: 1,
			pointStart: 0,
			pointInterval: paso,
			dataGrouping: [{dateTimeLabelFormats: 'second'}]
		}]
	});


	$('#'+glob+"canalY").highcharts('StockChart', {
		rangeSelector: {
			selected: 4,
			inputEnabled: false,
			buttonTheme: {
				visibility: 'hidden'
			},
			labelStyle: {
				visibility: 'hidden'
			}
		},
		chart: {
			height: 250,
			width: 600,
			type: 'line'
		},
		navigator: {
			enabled: false
		},
		title: {
			text: 'Canal Y'
		},
		xAxis: [{tickInterval: 1000}],
		yAxis: [{
			labels: {
				align: 'left'
			},
			title: {
				text: 'G'
			}
		}, {
			labels: {
				align: 'right'
			},
			title: {
				text: 'etiqueta'
			}
		}],

		series: [{
			type: 'line',
			name: 'mV',
			data: canalY2,
			pointStart: 0,
			yAxis: 0,
			pointInterval: paso,
			dataGrouping: [{dateTimeLabelFormats: 'second'}]
		}, {
			type: 'line',
			name: 'etiqueta',
			data: datos_etiquetado_aux,
			yAxis: 1,
			pointStart: 0,
			pointInterval: paso,
			dataGrouping: [{dateTimeLabelFormats: 'second'}]
		}]
	});

	$('#'+glob+"canalZ").highcharts('StockChart', {
		rangeSelector: {
			selected: 4,
			inputEnabled: false,
			buttonTheme: {
				visibility: 'hidden'
			},
			labelStyle: {
				visibility: 'hidden'
			}
		},
		chart: {
			height: 250,
			width: 600,
			type: 'line'
		},
		navigator: {
			enabled: false
		},
		title: {
			text: 'Canal Z'
		},
		xAxis: [{tickInterval: 1000}],
		yAxis: [{
			labels: {
				align: 'left'
			},
			title: {
				text: 'G'
			}
		}, {
			labels: {
				align: 'right'
			},
			title: {
				text: 'etiqueta'
			}
		}],

		series: [{
			type: 'line',
			name: 'mV',
			data: canalZ2,
			pointStart: 0,
			yAxis: 0,
			pointInterval: paso,
			dataGrouping: [{dateTimeLabelFormats: 'second'}]
		}, {
			type: 'line',
			name: 'etiqueta',
			data: datos_etiquetado_aux,
			yAxis: 1,
			pointStart: 0,
			pointInterval: paso,
			dataGrouping: [{dateTimeLabelFormats: 'second'}]
		}]
	});

}
*/
