
function funcion(){
	document.getElementById('file').addEventListener('change', cargar, false);

}

var canalesC = [];

function cargarOtros(evt) {

	var files2 = evt.target.files;
	//Eliminamos hijos
	var myNode = document.getElementById("izq");
	while (myNode.firstChild) {
   		myNode.removeChild(myNode.firstChild);
	}
	myNode = document.getElementById("der");
	while (myNode.firstChild) {
   		myNode.removeChild(myNode.firstChild);
	}
	
	//Cargar
	
	var num_canaleC = 0;
	var canalesC = [];

	var time = 0;
	var tiempo_grabacion = 0;
	var done = 0;

	var nombre = "";
	if (files2.length != 3) alert("Seleccionar un fichero .xml, otro .dat y otro .csv.");
	else {

		var nombre1 = files2[0].name;
		var nombre2 = files2[1].name;
		var nombre3 = files2[2].name;
		var nom1 = nombre1.split(".");
		var nom2 = nombre2.split(".");
		var nom3 = nombre3.split(".");

		if ((nom1[1] == "xml" && nom2[1] == "dat" && nom3[1] == "csv") || (nom1[1] == "dat" && nom2[1] == "xml" && nom3[1] == "csv") || (nom1[1] == "csv" && nom2[1] == "xml" && nom3[1] == "dat") || (nom1[1] == "dat" && nom2[1] == "csv" && nom3[1] == "xml") || (nom1[1] == "csv" && nom2[1] == "dat" && nom3[1] == "xml") || (nom1[1] == "xml" && nom2[1] == "csv" && nom3[1] == "dat")) {
			if (nom1[0] != nom2[0] || nom1[0] != nom3[0]) alert("Seleccionar tres ficheros con el mismo nombre.");
			else {
				//Lo primero es eliminar lo que hubiera antes
				//Cargamos los ficheros
				for (var i = 0, f; f = files2[i]; i++) {
					nombre = f.name;
					var nom = nombre.split(".");
					if(nom[1] == "xml"){

						var reader = new FileReader();
						reader.onload = function (e) {
							//Contenido cabecera:
							var p1 = e.target.result.split('>');
							var element = document.getElementById("file");

							var dd = document.createElement("div");
							dd.setAttribute("id", "underline");
							var uu = document.createElement("h2");
							uu.setAttribute("class", "underline");
							uu.textContent = nom1[0];
							dd.appendChild(uu);
							document.getElementById("izq").appendChild(dd);


							var fecha = document.createElement("p");
							fecha.textContent = "FECHA: "+p1[3].split('<')[0];
							document.getElementById("izq").appendChild(fecha);

							var tiempo = document.createElement("p");
							tiempo.textContent = "DURACIÓN: "+p1[5].split('<')[0];
							document.getElementById("izq").appendChild(tiempo);

							var conf = document.createElement("p");
							conf.textContent = "CONFIGURACIÓN: "+p1[7].split('<')[0];
							document.getElementById("izq").appendChild(fecha);

							var freq = document.createElement("p");
							freq.textContent = "FRECUENCIA CANALES: "+p1[10].split('<')[0];
							document.getElementById("izq").appendChild(freq);

							var freqA = document.createElement("p");
							freqA.textContent = "FRECUENCIA ACELERÓMETRO: "+p1[12].split('<')[0];
							document.getElementById("izq").appendChild(freqA);

							var ud = document.createElement("p");
							ud.textContent = "UNIDADES CANALES: "+p1[16].split('<')[0];
							document.getElementById("izq").appendChild(ud);

							var udA = document.createElement("p");
							udA.textContent = "UNIDADES ACELERÓMETRO: "+p1[18].split('<')[0];
							document.getElementById("izq").appendChild(udA);

							var formato = document.createElement("p");
							formato.textContent = "FORMATO DE DATOS: "+p1[21].split('<')[0];
							document.getElementById("izq").appendChild(formato);

							var can = document.createElement("p");
							can.textContent = "NÚMERO DE CANALES: "+p1[23].split('<')[0];
							document.getElementById("izq").appendChild(can);

							var k = 0; var aux = 24;
							while(k<parseInt(p1[23].split('<')[0])){
								var chan = document.createElement("p");
								aux += 2;
								chan.textContent = " > CANAL "+p1[aux].split('<')[0]+ " AJUSTES: " +p1[aux+2].split('<')[0];
								document.getElementById("izq").appendChild(chan);
								aux += 2;
								k+=1;
							}
							var ano = document.createElement("p");
							ano.textContent = "ANOTACIONES: "+p1[aux+3].split('<')[0];
							document.getElementById("izq").appendChild(ano);

							var dd = document.createElement("div");
							dd.setAttribute("id", "underline");
							var uu = document.createElement("h2");
							uu.setAttribute("class", "underline");
							uu.textContent = "Cargar otro"
							dd.appendChild(uu);
							document.getElementById("izq").appendChild(dd);

							var input = document.createElement("input");
							input.setAttribute("type", "file");
							input.setAttribute("id", "filesOtro");
							input.setAttribute("multiple", "multiple");
							input.setAttribute("name", "files[]");
							document.getElementById("izq").appendChild(input);

							document.getElementById("filesOtro").addEventListener('change', cargarOtros, false);

							tiempo_grabacion = parseFloat(p1[5].split('<')[0]);

							time = p1[5].split('<')[0].split(' ')[0];

							//Array canales:
							var doc = e.target.result;
							var frags = doc.split("\n"); var j=0; var canalesArchivo=0;
							while(j<frags.length){

								if(j==14){
									var c = frags[j].split(">");
									var ca = c[1].split("<");
									num_canaleC = parseInt(ca[0]);
								}

								if(canalesArchivo<num_canaleC && j>16 && j%2!=0){
									canalesArchivo++;
									var can = frags[j].split(">");
									var cana = can[1].split("<");
									canalesC.push(parseInt(cana[0]));
								}

								j++;
							}
						};
						reader.readAsText(f);


					}
				}

				for (var i = 0, f; f = files2[i]; i++) {
					nombre = f.name;
					var nom = nombre.split(".");

					if(nom[1]=="csv"){
						var reader = new FileReader();
						var datos_etiquetado;
						reader.onload = function (c) {
							datos_etiquetado = c.target.result.split("\n");
						};
						reader.readAsText(f);
					}
				}

				for (var i = 0, f; f = files2[i]; i++) {
					nombre = f.name;
					var nom = nombre.split(".");
					if(nom[1] == "dat"){

						var dd = document.createElement("div");
						dd.setAttribute("id", "underline");
						var uu = document.createElement("h2");
						uu.setAttribute("class", "underline");
						uu.textContent = "Canales"
						dd.appendChild(uu);
						document.getElementById("der").appendChild(dd);

						var table = document.createElement("table");
						table.setAttribute("width", "100%");
						document.getElementById("der").appendChild(table);
						var tr = document.createElement("tr");
						var tr2 = document.createElement("tr");
						var td1 = document.createElement("td");
						var td2 = document.createElement("td");
						var td3 = document.createElement("td");
						td1.setAttribute("valign","top");
						td2.setAttribute("valign","top");
						td3.setAttribute("valign","top");
						td3.setAttribute("id","td3");
						td3.setAttribute("colspan", "2");
						td2.setAttribute("id","td2");
						td1.setAttribute("id","td1");
						

						var readerD = new FileReader();
						var acc=nom[0].split("A");

						if(acc[0]=="C"){

							var dd = document.createElement("div");
							dd.setAttribute("id", "underline");
							var uu = document.createElement("h2");
							uu.setAttribute("class", "underline");
							uu.textContent = "Acelerómetro";
							dd.appendChild(uu);
							td3.appendChild(dd);

							var divX = document.createElement("div");
							divX.setAttribute("id","canalX");
							divX.setAttribute("style","float: left;");
							var divY = document.createElement("div");
							divY.setAttribute("id","canalY");
							divY.setAttribute("style","float: left;");
							var divZ = document.createElement("div");
							divZ.setAttribute("id","canalZ");
							divZ.setAttribute("style","float: left;");
							td3.appendChild(divX);
							td3.appendChild(divY);
							td3.appendChild(divZ);
						}

						readerD.onload = function (d) {

							var k = 0;
							while(k<canalesC.length){ 

								var tiempo = [];
								var tiempoA = [];
								var div = document.createElement("div");
								div.setAttribute("id","canal"+k);
								div.setAttribute("style", "display:block;");
								if(k<4){
									td1.appendChild(div);
								}else{
									td2.appendChild(div);
								}

								var aux = parseInt(canalesC[k]);
								var canal = [];
								var canalX = [];
								var canalY = [];
								var canalZ = [];
								var numeros = d.target.result.split("\n");
								var h=0; var flagAccel = 0; nums_chan = 0; nums_accel = 0;

								while(h<numeros.length){
									if(h == aux-1 && flagAccel<80) { //Guardamos los datos del canal k
										canal.push(numeros[h]);
										aux = aux + 8;
										nums_chan = nums_chan + 1;
									}
									if(flagAccel>79) { //Si hay acelerómetro
										if(acc[0]=="C" && done == 0){ //Guardamos los datos de los 3
											if(flagAccel==80 && numeros[h] != 'undefined') {canalX.push(numeros[h]); nums_accel = nums_accel + 1;}
											if(flagAccel==81 && numeros[h] != 'undefined') {canalY.push(numeros[h]); nums_accel = nums_accel + 1;}
											if(flagAccel==82 && numeros[h] != 'undefined') {canalZ.push(numeros[h]); nums_accel = nums_accel + 1;}
										}
										aux++;
									}

									h++;
									flagAccel++;
									if(flagAccel == 83) flagAccel=0;
								}


								//Igualamos el numero de etiquetas al numero de datos del canal
								var datos_etiquetado_aux = []
								var contador = 0, i = 0;
								while(datos_etiquetado_aux.length<canal.length){
									datos_etiquetado_aux.push(parseInt(datos_etiquetado[i]));
									i += 1;
								}


								var datosXsec = canal.length/parseInt(time);
								var paso = time/canal.length;
								var datosPasados = 0;
								var sec = 0;
								tiempo.push(0);
								while(tiempo.length<canal.length/2){
									if (datosPasados < datosXsec){
										datosPasados += 1;
										tiempo.push("");
									}
									else {
										sec = sec + 1;
										tiempo.push(sec);
										datosPasados = 0;
									}
								}

								var u=0, canalAux = [];
								while(u<canal.length){
									var num= parseFloat(canal[u]);
									canalAux.push(num);
									u= u+1;
								}

								$("#canal"+k).highcharts('StockChart', {
									rangeSelector: {
										selected: 4,
										inputEnabled: false,
										buttonTheme: {
											visibility: 'hidden'
										},
										labelStyle: {
											visibility: 'hidden'
										}
									},
									scrollbar : {
										enabled : false
									},
									credits: {
									    enabled: false
									},

									navigator: {
										enabled: false
									},
									chart: {
										height: 200,
										width: 423,
										type: 'line',
										spacingTop: -30,
										backgroundColor:'#000',
										style: {
									    		color: "#FFF"
										}

									},
									title: {
										style: {
											color: '#FFF'
										},
										text: 'Canal '+canalesC[k],
										y: 60
									},
									xAxis: [{
										tickInterval: 1000,
									        labels: {
											style: {
												color: '#FFF',
												font: '11px'
											}
										},
									}],
									yAxis: [{
										labels: {
											align: 'left',
										style: {
											color: '#FFF',
											font: '11px'
										}
										},
										title: {
											text: 'mV'
										}
									}, {
										labels: {
											align: 'right',
										style: {
											color: '#FFF',
											font: '11px'
										}
										},
										title: {
											text: 'etiqueta'
										}
									}],
									series: [{
										type: 'line',
										name: 'mV',
										data: canalAux,
										pointStart: 0,
										yAxis: 0,
										pointInterval: paso*1000,
										dataGrouping: [{dateTimeLabelFormats: 'second'}]
									}, {
										type: 'line',
										name: 'etiqueta',
										data: datos_etiquetado_aux,
										yAxis: 1,
										pointStart: 0,
										color: '#bbb000',
										pointInterval: paso*1000,
										dataGrouping: [{dateTimeLabelFormats: 'second'}]
									}]
								});


								k++;

							}
							 if(acc[0]=="C" && done==0){
							 done = 1;
							 var timeAux = (nums_accel / 3) / time;
							 var timeA = parseInt(timeAux);
							 var aux = 0; tiempoA.push("0"); var secs=1; var aux2 = 1;
							 while(aux < nums_accel / 3){
							 if(aux2 < timeA){
							 tiempoA.push("");
							 aux2 = aux2 + 1;
							 }else{
							 tiempoA.push(secs);
							 aux2 = 0;
							 secs = secs + 1;
							 }
							 aux= aux + 1;
							 }
							 cargarAccel(canalX,canalY,canalZ, tiempo_grabacion, datos_etiquetado);
							 }
						};

						tr.appendChild(td1);
						tr.appendChild(td2);
						tr2.appendChild(td3);
						table.appendChild(tr);
						table.appendChild(tr2);

						readerD.readAsText(f);
					}
				}
			}
		}
		else alert("Seleccionar un fichero .xml, otro .dat y otro .csv.");
	}
}

function cargar(evt) {
	var files = evt.target.files;

	var num_canaleC = 0;

	var time = 0;
	var tiempo_grabacion = 0;
	var done = 0;

	var nombre = "";
	if (files.length != 3) alert("Seleccionar un fichero .xml, otro .dat y otro .csv.");
	else {

		var nombre1 = files[0].name;
		var nombre2 = files[1].name;
		var nombre3 = files[2].name;
		var nom1 = nombre1.split(".");
		var nom2 = nombre2.split(".");
		var nom3 = nombre3.split(".");

		if ((nom1[1] == "xml" && nom2[1] == "dat" && nom3[1] == "csv") || (nom1[1] == "dat" && nom2[1] == "xml" && nom3[1] == "csv") || (nom1[1] == "csv" && nom2[1] == "xml" && nom3[1] == "dat") || (nom1[1] == "dat" && nom2[1] == "csv" && nom3[1] == "xml") || (nom1[1] == "csv" && nom2[1] == "dat" && nom3[1] == "xml") || (nom1[1] == "xml" && nom2[1] == "csv" && nom3[1] == "dat")) {
			if (nom1[0] != nom2[0] || nom1[0] != nom3[0]) alert("Seleccionar tres ficheros con el mismo nombre.");
			else {
				//Lo primero es eliminar lo que hubiera antes
				//Cargamos los ficheros
				for (var i = 0, f; f = files[i]; i++) {
					nombre = f.name;
					var nom = nombre.split(".");
					if(nom[1] == "xml"){

						var reader = new FileReader();
						reader.onload = function (e) {
							//Contenido cabecera:
							var p1 = e.target.result.split('>');
							var element = document.getElementById("file");
							element.parentNode.removeChild(element);

							var dd = document.createElement("div");
							dd.setAttribute("id", "underline");
							var uu = document.createElement("h2");
							uu.setAttribute("class", "underline");
							uu.textContent = nom1[0];
							dd.appendChild(uu);
							document.getElementById("izq").appendChild(dd);

							var fecha = document.createElement("p");
							fecha.textContent = "FECHA: "+p1[3].split('<')[0];
							document.getElementById("izq").appendChild(fecha);

							var tiempo = document.createElement("p");
							tiempo.textContent = "DURACIÓN: "+p1[5].split('<')[0];
							document.getElementById("izq").appendChild(tiempo);

							var conf = document.createElement("p");
							conf.textContent = "CONFIGURACIÓN: "+p1[7].split('<')[0];
							document.getElementById("izq").appendChild(fecha);

							var freq = document.createElement("p");
							freq.textContent = "FRECUENCIA CANALES: "+p1[10].split('<')[0];
							document.getElementById("izq").appendChild(freq);

							var freqA = document.createElement("p");
							freqA.textContent = "FRECUENCIA ACELERÓMETRO: "+p1[12].split('<')[0];
							document.getElementById("izq").appendChild(freqA);

							var ud = document.createElement("p");
							ud.textContent = "UNIDADES CANALES: "+p1[16].split('<')[0];
							document.getElementById("izq").appendChild(ud);

							var udA = document.createElement("p");
							udA.textContent = "UNIDADES ACELERÓMETRO: "+p1[18].split('<')[0];
							document.getElementById("izq").appendChild(udA);

							var formato = document.createElement("p");
							formato.textContent = "FORMATO DE DATOS: "+p1[21].split('<')[0];
							document.getElementById("izq").appendChild(formato);

							var can = document.createElement("p");
							can.textContent = "NÚMERO DE CANALES: "+p1[23].split('<')[0];
							document.getElementById("izq").appendChild(can);

							var k = 0; var aux = 24;
							while(k<parseInt(p1[23].split('<')[0])){
								var chan = document.createElement("p");
								aux += 2;
								chan.textContent = " > CANAL "+p1[aux].split('<')[0]+ " AJUSTES: " +p1[aux+2].split('<')[0];
								document.getElementById("izq").appendChild(chan);
								aux += 2;
								k+=1;
							}

							var ano = document.createElement("p");
							ano.textContent = "ANOTACIONES: "+p1[aux+3].split('<')[0];
							document.getElementById("izq").appendChild(ano);


							var dd = document.createElement("div");
							dd.setAttribute("id", "underline");
							var uu = document.createElement("h2");
							uu.setAttribute("class", "underline");
							uu.textContent = "Cargar otro"
							dd.appendChild(uu);
							document.getElementById("izq").appendChild(dd);

							var input = document.createElement("input");
							input.setAttribute("type", "file");
							input.setAttribute("id", "filesOtro");
							input.setAttribute("multiple", "multiple");
							input.setAttribute("name", "files[]");
							document.getElementById("izq").appendChild(input);

							document.getElementById("filesOtro").addEventListener('change', cargarOtros, false);

							tiempo_grabacion = parseFloat(p1[5].split('<')[0]);

							time = p1[5].split('<')[0].split(' ')[0];

							//Array canales:
							var doc = e.target.result;
							var frags = doc.split("\n"); var j=0; var canalesArchivo=0;
							while(j<frags.length){

								if(j==14){
									var c = frags[j].split(">");
									var ca = c[1].split("<");
									num_canaleC = parseInt(ca[0]);
								}

								if(canalesArchivo<num_canaleC && j>16 && j%2!=0){
									canalesArchivo++;
									var can = frags[j].split(">");
									var cana = can[1].split("<");
									canalesC.push(parseInt(cana[0]));
								}

								j++;
							}
						};
						reader.readAsText(f);


					}
				}

				for (var i = 0, f; f = files[i]; i++) {
					nombre = f.name;
					var nom = nombre.split(".");

					if(nom[1]=="csv"){
						var reader = new FileReader();
						var datos_etiquetado;
						reader.onload = function (c) {
							datos_etiquetado = c.target.result.split("\n");
						};
						reader.readAsText(f);
					}
				}

				for (var i = 0, f; f = files[i]; i++) {
					nombre = f.name;
					var nom = nombre.split(".");
					if(nom[1] == "dat"){

						var dd = document.createElement("div");
						dd.setAttribute("id", "underline");
						var uu = document.createElement("h2");
						uu.setAttribute("class", "underline");
						uu.textContent = "Canales"
						dd.appendChild(uu);
						document.getElementById("der").appendChild(dd);

						var table = document.createElement("table");
						table.setAttribute("width", "100%");
						document.getElementById("der").appendChild(table);
						var tr = document.createElement("tr");
						var tr2 = document.createElement("tr");
						var td1 = document.createElement("td");
						var td2 = document.createElement("td");
						var td3 = document.createElement("td");
						td1.setAttribute("valign","top");
						td2.setAttribute("valign","top");
						td3.setAttribute("valign","top");
						td3.setAttribute("id","td3");
						td3.setAttribute("colspan", "2");
						td2.setAttribute("id","td2");
						td1.setAttribute("id","td1");


						var readerD = new FileReader();
						var acc=nom[0].split("A");

						if(acc[0]=="C"){

							var dd = document.createElement("div");
							dd.setAttribute("id", "underline");
							var uu = document.createElement("h2");
							uu.setAttribute("class", "underline");
							uu.textContent = "Acelerómetro";
							dd.appendChild(uu);
							td3.appendChild(dd);

							var divX = document.createElement("div");
							divX.setAttribute("id","canalX");
							divX.setAttribute("style","float: left;");
							var divY = document.createElement("div");
							divY.setAttribute("id","canalY");
							divY.setAttribute("style","float: left;");
							var divZ = document.createElement("div");
							divZ.setAttribute("id","canalZ");
							divZ.setAttribute("style","float: left;");
							td3.appendChild(divX);
							td3.appendChild(divY);
							td3.appendChild(divZ);
						}

						readerD.onload = function (d) {


							var k = 0;
							while(k<canalesC.length){

								var tiempo = [];
								var tiempoA = [];
								var div = document.createElement("div");
								div.setAttribute("id","canal"+k);
								div.setAttribute("style", "display:block;");
								if(k<4){
									td1.appendChild(div);
								}else{
									td2.appendChild(div);
								}

								var aux = parseInt(canalesC[k]);
								var canal = [];
								var canalX = [];
								var canalY = [];
								var canalZ = [];
								var numeros = d.target.result.split("\n");
								var h=0; var flagAccel = 0; nums_chan = 0; nums_accel = 0;

								while(h<numeros.length){
									if(h == aux-1 && flagAccel<80) { //Guardamos los datos del canal k
										canal.push(numeros[h]);
										aux = aux + 8;
										nums_chan = nums_chan + 1;
									}
									if(flagAccel>79) { //Si hay acelerómetro
										if(acc[0]=="C" && done == 0){ //Guardamos los datos de los 3
											if(flagAccel==80 && numeros[h] != 'undefined') {canalX.push(numeros[h]); nums_accel = nums_accel + 1;}
											if(flagAccel==81 && numeros[h] != 'undefined') {canalY.push(numeros[h]); nums_accel = nums_accel + 1;}
											if(flagAccel==82 && numeros[h] != 'undefined') {canalZ.push(numeros[h]); nums_accel = nums_accel + 1;}
										}
										aux++;
									}

									h++;
									flagAccel++;
									if(flagAccel == 83) flagAccel=0;
								}


								//Igualamos el numero de etiquetas al numero de datos del canal
								var datos_etiquetado_aux = []
								var contador = 0, i = 0;
								while(datos_etiquetado_aux.length<canal.length){
									datos_etiquetado_aux.push(parseInt(datos_etiquetado[i]));
									i += 1;
								}


								var datosXsec = canal.length/parseInt(time);
								var paso = time/canal.length;
								var datosPasados = 0;
								var sec = 0;
								tiempo.push(0);
								while(tiempo.length<canal.length/2){
									if (datosPasados < datosXsec){
										datosPasados += 1;
										tiempo.push("");
									}
									else {
										sec = sec + 1;
										tiempo.push(sec);
										datosPasados = 0;
									}
								}

								var u=0, canalAux = [];
								while(u<canal.length){
									var num= parseFloat(canal[u]);
									canalAux.push(num);
									u= u+1;
								}

								$("#canal"+k).highcharts('StockChart', {
									rangeSelector: {
										selected: 4,
										inputEnabled: false,
										buttonTheme: {
											visibility: 'hidden'
										},
										labelStyle: {
											visibility: 'hidden'
										}
									},
									scrollbar : {
										enabled : false
									},
									credits: {
									    enabled: false
									},
									navigator: {
										enabled: false
									},
									chart: {
										height: 200,
										width: 423,
										type: 'line',
										spacingTop: -30,
										backgroundColor:'#000',
										style: {
									    		color: "#FFF"
										}

									},
									title: {
										style: {
											color: '#FFF'
										},
										text: 'Canal '+canalesC[k],
										y: 60
									},
									xAxis: [{
										tickInterval: 1000,
									        labels: {
											style: {
												color: '#FFF',
												font: '11px'
											}
										},
									}],
									yAxis: [{
										labels: {
											align: 'left',
										style: {
											color: '#FFF',
											font: '11px'
										}
										},
										title: {
											text: 'mV'
										}
									}, {
										labels: {
											align: 'right',
										style: {
											color: '#FFF',
											font: '11px'
										}
										},
										title: {
											text: 'etiqueta'
										}
									}],

									series: [{
										type: 'line',
										name: 'mV',
										data: canalAux,
										pointStart: 0,
										yAxis: 0,
										pointInterval: paso*1000,
										dataGrouping: [{dateTimeLabelFormats: 'second'}]
									}, {
										type: 'line',
										name: 'etiqueta',
										data: datos_etiquetado_aux,
										yAxis: 1,
										pointStart: 0,
										color: '#bbb000',
										pointInterval: paso*1000,
										dataGrouping: [{dateTimeLabelFormats: 'second'}]
									}]
								});


								k++;

							}
							 if(acc[0]=="C" && done==0){
							 done = 1;
							 var timeAux = (nums_accel / 3) / time;
							 var timeA = parseInt(timeAux);
							 var aux = 0; tiempoA.push("0"); var secs=1; var aux2 = 1;
							 while(aux < nums_accel / 3){
							 if(aux2 < timeA){
							 tiempoA.push("");
							 aux2 = aux2 + 1;
							 }else{
							 tiempoA.push(secs);
							 aux2 = 0;
							 secs = secs + 1;
							 }
							 aux= aux + 1;
							 }
							 cargarAccel(canalX,canalY,canalZ, tiempo_grabacion, datos_etiquetado);
							 }
						};

						tr.appendChild(td1);
						tr.appendChild(td2);
						tr2.appendChild(td3);
						table.appendChild(tr);
						table.appendChild(tr2);

						readerD.readAsText(f);
					}
				}
			}
		}
		else alert("Seleccionar un fichero .xml, otro .dat y otro .csv.");
	}
}


function cargarAccel(canalX,canalY,canalZ, duracion, datos_etiquetado){

	var datos_etiquetado_aux = []

	//Igualamos el numero de etiquetas al numero de datos del acelerometro
	var contador = 0, i = 0;
	while(datos_etiquetado_aux.length!=canalX.length){
		if(contador < 10) {
			contador += 1;
		}else{
			datos_etiquetado_aux.push(parseInt(datos_etiquetado[i]));
			contador = 0;
		}
		i += 1;

	}

	var paso = duracion/canalZ.length;


	var u=0, canalX2 = [];
	while(u<canalX.length){
		var num= parseFloat(canalX[u]);
		canalX2.push(num);
		u= u+1;
	}
	u=0; var canalY2 = [];
	while(u<canalY.length){
		var num= parseFloat(canalY[u]);
		canalY2.push(num);
		u= u+1;
	}
	u=0; var canalZ2 = [];
	while(u<canalZ.length){
		var num= parseFloat(canalZ[u]);
		canalZ2.push(num);
		u= u+1;
	}

	$("#canalX").highcharts('StockChart', {
		rangeSelector: {
			selected: 4,
			inputEnabled: false,
			buttonTheme: {
				visibility: 'hidden'
			},
			labelStyle: {
				visibility: 'hidden'
			}
		},
		scrollbar : {
			enabled : false
		},
		credits: {
			enabled: false
		},
		chart: {
			height: 160,
			width: 300,
			type: 'line',
			spacingBottom: 2,
			spacingTop: -30,
			spacingLeft: 2,
			spacingRight: 2,
			backgroundColor:'#000',
			style: {
			    	color: "#FFF"
			}
		},
		navigator: {
			enabled: false
		},
		title: {
			style: {
				color: '#FFF'
			},
			text: 'Canal X',
			y: 60
		},
		xAxis: [{
			tickInterval: 1000,
			labels: {
				style: {
					color: '#FFF',
					font: '11px'
				}
			},
		}],
		yAxis: [{
			labels: {
				align: 'left',
				style: {
					color: '#FFF',
					font: '11px'
				}
			},
			title: {
				text: 'G'
			}
		}, {
			labels: {
				align: 'right',
				style: {
					color: '#FFF',
					font: '11px'
				}
			},
			title: {
				text: 'etiqueta'
			}
		}],

		series: [{
			type: 'line',
			name: 'mV',
			data: canalX2,
			pointStart: 0,
			yAxis: 0,
			pointInterval: paso*1000,
			dataGrouping: [{dateTimeLabelFormats: 'second'}]
		}, {
			type: 'line',
			name: 'etiqueta',
			data: datos_etiquetado_aux,
			yAxis: 1,
			pointStart: 0,
			color: '#bbb000',
			pointInterval: paso*1000,
			dataGrouping: [{dateTimeLabelFormats: 'second'}]
		}]
	});


	$('#canalY').highcharts('StockChart', {
		rangeSelector: {
			selected: 4,
			inputEnabled: false,
			buttonTheme: {
				visibility: 'hidden'
			},
			labelStyle: {
				visibility: 'hidden'
			}
		},
		scrollbar : {
			enabled : false
		},
		credits: {
			enabled: false
		},
		chart: {
			height: 160,
			width: 300,
			type: 'line',
			spacingBottom: 2,
			spacingTop: -30,
			spacingLeft: 2,
			spacingRight: 2,
			backgroundColor:'#000',
			style: {
			    	color: "#FFF"
			}
		},
		navigator: {
			enabled: false
		},
		title: {
			style: {
				color: '#FFF'
			},
			text: 'Canal Y',
			y: 60
		},
		xAxis: [{
			tickInterval: 1000,
			labels: {
				style: {
					color: '#FFF',
					font: '11px'
				}
			},
		}],
		yAxis: [{
			labels: {
				align: 'left',
				style: {
					color: '#FFF',
					font: '11px'
				}
			},
			title: {
				text: 'G'
			}
		}, {
			labels: {
				align: 'right',
				style: {
					color: '#FFF',
					font: '11px'
				}
			},
			title: {
				text: 'etiqueta'
			}
		}],

		series: [{
			type: 'line',
			name: 'mV',
			data: canalY2,
			pointStart: 0,
			yAxis: 0,
			pointInterval: paso*1000,
			dataGrouping: [{dateTimeLabelFormats: 'second'}]
		}, {
			type: 'line',
			name: 'etiqueta',
			data: datos_etiquetado_aux,
			yAxis: 1,
			pointStart: 0,
			color: '#bbb000',
			pointInterval: paso*1000,
			dataGrouping: [{dateTimeLabelFormats: 'second'}]
		}]
	});

	$("#canalZ").highcharts('StockChart', {
		rangeSelector: {
			selected: 4,
			inputEnabled: false,
			buttonTheme: {
				visibility: 'hidden'
			},
			labelStyle: {
				visibility: 'hidden'
			}
		},
		scrollbar : {
			enabled : false
		},
		credits: {
			enabled: false
		},
		chart: {
			height: 160,
			width: 300,
			type: 'line',
			spacingBottom: 2,
			spacingTop: -30,
			spacingLeft: 2,
			spacingRight: 2,
			backgroundColor:'#000',
			style: {
			    	color: "#FFF"
			}
		},
		navigator: {
			enabled: false
		},
		title: {
			style: {
				color: '#FFF'
			},
			text: 'Canal Z',
			y: 60
		},
		xAxis: [{
			tickInterval: 1000,
			labels: {
				style: {
					color: '#FFF',
					font: '11px'
				}
			},
		}],
		yAxis: [{
			labels: {
				align: 'left',
				style: {
					color: '#FFF',
					font: '11px'
				}
			},
			title: {
				text: 'G'
			}
		}, {
			labels: {
				align: 'right',
				style: {
					color: '#FFF',
					font: '11px'
				}
			},
			title: {
				text: 'etiqueta'
			}
		}],

		series: [{
			type: 'line',
			name: 'mV',
			data: canalZ2,
			pointStart: 0,
			yAxis: 0,
			pointInterval: paso*1000,
			dataGrouping: [{dateTimeLabelFormats: 'second'}]
		}, {
			type: 'line',
			name: 'etiqueta',
			data: datos_etiquetado_aux,
			yAxis: 1,
			pointStart: 0,
			color: '#bbb000',
			pointInterval: paso*1000,
			dataGrouping: [{dateTimeLabelFormats: 'second'}]
		}]
	});

}

