function guardarG(){

	//Fichero cabecera
        var d = new Date(); 
      	var ficheroCabecera = new Array();
	ficheroCabecera.push('<?xml version="1.0" encoding="UTF-8"?>');
	ficheroCabecera.push('<cabecera>');
	ficheroCabecera.push('	<fecha>'+tiempo_sis+'</fecha>');
	ficheroCabecera.push('	<duracion>'+seconds+"."+milisec+' segundos'+'</duracion>');

	var num_canales = 0;
	var activos = "";
	for (var i = 0; i < todos_canales.length; i++)	{
		if(todos_canales[i].checked){
			num_canales++;
			activos = activos +" "+ todos_canales[i].value;
		}
	}


	var conf = document.getElementById("configuracion_canales");
	var confi = conf.value;

	ficheroCabecera.push('	<config>'+confi+'</config>');

	ficheroCabecera.push('	<freq>');
	ficheroCabecera.push('		<f-channel>250Hz</f-channel>');
	ficheroCabecera.push('		<f-accel>25Hz</f-accel>');
	ficheroCabecera.push('	</freq>');
	ficheroCabecera.push('	<units>');
	ficheroCabecera.push('		<u-channel>uV</u-channel>');
	ficheroCabecera.push('		<u-accel>G</u-accel>');
	ficheroCabecera.push('	</units>');
	ficheroCabecera.push('	<formato>Float 32 bits</formato>');
	ficheroCabecera.push('	<num_channels>'+num_canales+'</num_channels>');
	ficheroCabecera.push('	<channel-settings-mode>');
	ficheroCabecera.push(num_canales +" canales cuyos Channel Settings Mode son:");
	for (var i = 0; i < todos_canales.length; i++)	{
		if(todos_canales[i].checked){
			ficheroCabecera.push('		<name>'+todos_canales[i].value+'</name>');
			if(todos_canales[i].value == '1')
				ficheroCabecera.push('		<mode>'+chansettmode1+'</mode>');
			else if(todos_canales[i].value == '2')
				ficheroCabecera.push('		<mode>'+chansettmode2+'</mode>');
			else if(todos_canales[i].value == '3')
				ficheroCabecera.push('		<mode>'+chansettmode3+'</mode>');
			else if(todos_canales[i].value == '4')
				ficheroCabecera.push('		<mode>'+chansettmode4+'</mode>');
			else if(todos_canales[i].value == '5')
				ficheroCabecera.push('		<mode>'+chansettmode5+'</mode>');
			else if(todos_canales[i].value == '6')
				ficheroCabecera.push('		<mode>'+chansettmode6+'</mode>');
			else if(todos_canales[i].value == '7')
				ficheroCabecera.push('		<mode>'+chansettmode7+'</mode>');
			else if(todos_canales[i].value == '8')
				ficheroCabecera.push('		<mode>'+chansettmode8+'</mode>');
		}
	}
	ficheroCabecera.push('	</channel-settings-mode>');
	ficheroCabecera.push('	<anotaciones>'+ document.getElementById("anotaciones").value+'</anotaciones>');
		ficheroCabecera.push('</cabecera>');

        ficheroCabecera = ficheroCabecera.join("\n");

        var fileName = document.getElementById("nameFich").value + ".xml";
        var blob = new Blob([ficheroCabecera], {
       		type: "text/csv;charset=utf8;"
        });

	//Fichero datos
       	grabacion2 = grabacion2.join("\n")//;

       	var fileName2 = document.getElementById("nameFich").value + ".dat";
       	var blob2 = new Blob([grabacion2], {
       		type: "text/csv;charset=utf8;"
       	});

	//Fichero etiquetas
	grab_etiq = grab_etiq.join("\n")//;
       	var fileName3 = document.getElementById("nameFich").value + ".csv";
       	var blob3 = new Blob([grab_etiq], {
       		type: "text/csv;charset=utf8;"
       	});


	//Descargar
       	var downloadLink = document.createElement("a");
        downloadLink.setAttribute("href", window.URL.createObjectURL(blob));
        downloadLink.setAttribute("download", fileName); //atributo de descarga
        downloadLink.innerHTML = "◎ Guardar .xml";
        document.getElementById("aqui").appendChild(downloadLink);
	
	//Separacion
	var sep = document.createElement("a");
	sep.innerHTML = " || ";
	document.getElementById("aqui").appendChild(sep);
	//Descargar
	var downloadLink2 = document.createElement("a");
        downloadLink2.setAttribute("href", window.URL.createObjectURL(blob2));
        downloadLink2.setAttribute("download", fileName2); //atributo de descarga
        downloadLink2.innerHTML = "◎ Guardar .dat";
        document.getElementById("aqui").appendChild(downloadLink2);
	//Separacion
	var sep2 = document.createElement("a");
	sep2.innerHTML = " || ";
	document.getElementById("aqui").appendChild(sep2);
	//Descargar
	var downloadLink3 = document.createElement("a");
        downloadLink3.setAttribute("href", window.URL.createObjectURL(blob3));
        downloadLink3.setAttribute("download", fileName3); //atributo de descarga
        downloadLink3.innerHTML = "◎ Guardar .csv";
        document.getElementById("aqui").appendChild(downloadLink3);

	document.getElementById("grabar").disabled=false;
	document.getElementById("detener").disabled=true;

}
