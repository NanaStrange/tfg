	var si = [];
	var no = [];
	var ultimo = 0;

function func(){
	
	while (document.getElementById("graficas").firstChild) {
    		document.getElementById("graficas").removeChild(document.getElementById("graficas").firstChild);
	}

	while (document.getElementById("graficas2").firstChild) {
    		document.getElementById("graficas2").removeChild(document.getElementById("graficas2").firstChild);
	}
	
		var nombresCanal = new Array()
		var todos_canales=document.getElementsByName("canal");
		var num_canales=0;
		for (var i = 0; i < todos_canales.length; i++)	{
			if(todos_canales[i].checked){
				num_canales++;
				nombresCanal.push(String(i+1));
				no.push(i+1);
			}
			else{
				si.push(i+1);
			}
		}
	
		if(no.length == 1){
			ultimo = no[0];
		}
		si = [];
		no = [];

		if(num_canales != 0){

	function Timeline(hechos, nombreCanal) {

		
		this.data = new TimeSeries();
		//this.chart = new SmoothieChart({maxValue:20000,minValue:17000});
		this.chart = new SmoothieChart();

		var num = 0;
		for (var i = 0; i < todos_canales.length; i++)	{
			if(todos_canales[i].checked)
				num++;
		}

		if(num==1){
		var parrafo = document.createElement("p");
		var contenidoParrafo = document.createTextNode("Canal "+nombreCanal);
		parrafo.appendChild(contenidoParrafo);
		parrafo.style.marginTop="70px";
		parrafo.style.marginBottom="-50px";
		parrafo.style.position="relative";

		this.chart.addTimeSeries(this.data, { strokeStyle: 'rgba(0, 255, 0, 1)', fillStyle: 'rgba(0, 255, 0, 0.2)', lineWidth: 1 });
		var lienzo = document.createElement("canvas");
		lienzo.setAttribute("width", "1200");
		lienzo.setAttribute("height", "300");
		lienzo.style.marginTop="70px";
		lienzo.style.marginBottom="-50px";
		lienzo.style.position="relative";
		lienzo.style.zIndex="1";
		this.chart.streamTo(lienzo, 500);
		document.getElementById("graficas").appendChild(parrafo);
		document.getElementById("graficas").appendChild(lienzo);

		}
		

		else if(num==2){
		var parrafo = document.createElement("p");
		var contenidoParrafo = document.createTextNode("Canal "+nombreCanal);
		parrafo.appendChild(contenidoParrafo);
		parrafo.style.marginTop="60px";
		parrafo.style.marginBottom="-70px";
		parrafo.style.position="relative";

		this.chart.addTimeSeries(this.data, { strokeStyle: 'rgba(0, 255, 0, 1)', fillStyle: 'rgba(0, 255, 0, 0.2)', lineWidth: 1 });
		var lienzo = document.createElement("canvas");
		lienzo.setAttribute("width", "1200");
		lienzo.setAttribute("height", "250");
		lienzo.style.marginTop="-40px";
		lienzo.style.marginBottom="70px";
		lienzo.style.position="relative";
		lienzo.style.zIndex="-1";
		this.chart.streamTo(lienzo, 500);
		document.getElementById("graficas").appendChild(parrafo);
		document.getElementById("graficas").appendChild(lienzo);

		}
		

		else if(num==3){
		var parrafo = document.createElement("p");
		var contenidoParrafo = document.createTextNode("Canal "+nombreCanal);
		parrafo.appendChild(contenidoParrafo);
		parrafo.style.marginTop="60px";
		parrafo.style.marginBottom="-70px";
		parrafo.style.position="relative";

		this.chart.addTimeSeries(this.data, { strokeStyle: 'rgba(0, 255, 0, 1)', fillStyle: 'rgba(0, 255, 0, 0.2)', lineWidth: 1 });
		var lienzo = document.createElement("canvas");
		lienzo.setAttribute("width", "1200");
		lienzo.setAttribute("height", "200");
		lienzo.style.marginTop="-40px";
		lienzo.style.marginBottom="70px";
		lienzo.style.position="relative";
		lienzo.style.zIndex="-1";
		this.chart.streamTo(lienzo, 500);
		document.getElementById("graficas").appendChild(parrafo);
		document.getElementById("graficas").appendChild(lienzo);

		}
		

		else if(num==4){
		var parrafo = document.createElement("p");
		var contenidoParrafo = document.createTextNode("Canal "+nombreCanal);
		parrafo.appendChild(contenidoParrafo);
		parrafo.style.marginTop="60px";
		parrafo.style.marginBottom="-70px";
		parrafo.style.position="relative";

		this.chart.addTimeSeries(this.data, { strokeStyle: 'rgba(0, 255, 0, 1)', fillStyle: 'rgba(0, 255, 0, 0.2)', lineWidth: 1 });
		var lienzo = document.createElement("canvas");
		lienzo.setAttribute("width", "1200");
		lienzo.setAttribute("height", "170");
		lienzo.style.marginTop="-40px";
		lienzo.style.marginBottom="70px";
		lienzo.style.position="relative";
		lienzo.style.zIndex="-1";
		this.chart.streamTo(lienzo, 500);
		document.getElementById("graficas").appendChild(parrafo);
		document.getElementById("graficas").appendChild(lienzo);

		}
		

		else if(num==5){
		//Si hechos==3, cambio de columna
		var parrafo = document.createElement("p");
		var contenidoParrafo = document.createTextNode("Canal "+nombreCanal);
		parrafo.appendChild(contenidoParrafo);
		parrafo.style.marginTop="60px";
		parrafo.style.marginBottom="-70px";
		parrafo.style.position="relative";

		
		this.chart.addTimeSeries(this.data, { strokeStyle: 'rgba(0, 255, 0, 1)', fillStyle: 'rgba(0, 255, 0, 0.2)', lineWidth: 1 });

		var lienzo = document.createElement("canvas");
		lienzo.setAttribute("width", "600");
		lienzo.setAttribute("height", "200");
		lienzo.style.marginTop="-30px";
		lienzo.style.marginBottom="70px";
		lienzo.style.position="relative";
		lienzo.style.zIndex="-1";
		this.chart.streamTo(lienzo, 500);
		if(hechos<3){
			document.getElementById("graficas").appendChild(parrafo);
			document.getElementById("graficas").appendChild(lienzo);
		}
		else {
			lienzo.style.marginLeft="20px";
			parrafo.style.marginLeft="20px";
			document.getElementById("graficas2").appendChild(parrafo);
			document.getElementById("graficas2").appendChild(lienzo);
		}

		}


		else if(num==6){
		//Si hechos==3, cambio de columna
		var parrafo = document.createElement("p");
		var contenidoParrafo = document.createTextNode("Canal "+nombreCanal);
		parrafo.appendChild(contenidoParrafo);
		parrafo.style.marginTop="60px";
		parrafo.style.marginBottom="-70px";
		parrafo.style.position="relative";

		
		this.chart.addTimeSeries(this.data, { strokeStyle: 'rgba(0, 255, 0, 1)', fillStyle: 'rgba(0, 255, 0, 0.2)', lineWidth: 1 });

		var lienzo = document.createElement("canvas");
		lienzo.setAttribute("width", "600");
		lienzo.setAttribute("height", "200");
		lienzo.style.marginTop="-30px";
		lienzo.style.marginBottom="70px";
		lienzo.style.position="relative";
		lienzo.style.zIndex="-1";
		this.chart.streamTo(lienzo, 500);
		if(hechos<3){
			document.getElementById("graficas").appendChild(parrafo);
			document.getElementById("graficas").appendChild(lienzo);
		}
		else {
			lienzo.style.marginLeft="20px";
			parrafo.style.marginLeft="20px";
			document.getElementById("graficas2").appendChild(parrafo);
			document.getElementById("graficas2").appendChild(lienzo);
		}

		}

		else if(num==7){
		//Si hechos==4, cambio de columna
		var parrafo = document.createElement("p");
		var contenidoParrafo = document.createTextNode("Canal "+nombreCanal);
		parrafo.appendChild(contenidoParrafo);
		parrafo.style.marginTop="60px";
		parrafo.style.marginBottom="-70px";
		parrafo.style.position="relative";

		
		this.chart.addTimeSeries(this.data, { strokeStyle: 'rgba(0, 255, 0, 1)', fillStyle: 'rgba(0, 255, 0, 0.2)', lineWidth: 1 });

		var lienzo = document.createElement("canvas");
		lienzo.setAttribute("width", "600");
		lienzo.setAttribute("height", "150");
		lienzo.style.marginTop="-30px";
		lienzo.style.marginBottom="70px";
		lienzo.style.position="relative";
		lienzo.style.zIndex="-1";
		this.chart.streamTo(lienzo, 500);
		if(hechos<4){
			document.getElementById("graficas").appendChild(parrafo);
			document.getElementById("graficas").appendChild(lienzo);
		}
		else {
			lienzo.style.marginLeft="20px";
			parrafo.style.marginLeft="20px";
			document.getElementById("graficas2").appendChild(parrafo);
			document.getElementById("graficas2").appendChild(lienzo);
		}

		}
		

		else if(num==8){
		//Si hechos==4, cambio de columna
		var parrafo = document.createElement("p");
		var contenidoParrafo = document.createTextNode("Canal "+nombreCanal);
		parrafo.appendChild(contenidoParrafo);
		parrafo.style.marginTop="60px";
		parrafo.style.marginBottom="-75px";
		parrafo.style.position="relative";

		
		this.chart.addTimeSeries(this.data, { strokeStyle: 'rgba(0, 255, 0, 1)', fillStyle: 'rgba(0, 255, 0, 0.2)', lineWidth: 1 });

		var lienzo = document.createElement("canvas");
		lienzo.setAttribute("width", "600");
		lienzo.setAttribute("height", "150");
		lienzo.style.marginTop="-30px";
		lienzo.style.marginBottom="70px";
		lienzo.style.position="relative";
		lienzo.style.zIndex="-1";
		this.chart.streamTo(lienzo, 500);
		if(hechos<4){
			document.getElementById("graficas").appendChild(parrafo);
			document.getElementById("graficas").appendChild(lienzo);
		}
		else {
			lienzo.style.marginLeft="20px";
			parrafo.style.marginLeft="20px";
			document.getElementById("graficas2").appendChild(parrafo);
			document.getElementById("graficas2").appendChild(lienzo);
		}

		}
	}


	Timeline.prototype.sample = function (s) {
		this.data.append(new Date().getTime(), s);
	}

	var nombresCanal = new Array()
	var todos_canales=new Array()
	todos_canales=document.getElementsByName("canal");
	var num_canales=0;
	for (var i = 0; i < todos_canales.length; i++)	{
		if(todos_canales[i].checked){
			num_canales++;
			nombresCanal.push(String(i+1));
		}
	}

	var waves = [];
	for (var i = 0; i < num_canales; i++) { //Numero de canales
		waves.push(new Timeline(i, nombresCanal[i]));
	}

	var j=0; var cont_accel = 0; var cont_accel2 = 0; var empezar = 0;

	var socket = io.connect('http://127.0.0.1');
		socket.on('openbci', function (sample) { 
				j=0;
				for(var i=0; i<8; i++){
					if(todos_canales[i].checked){
						var dato = sample[i];
						waves[j].sample(dato);
						j+=1;
					}
				}
				if(cont_accel > 4 && cont_accel < 11){
					if(sample[8] != 0 && sample[9] != 0 && sample[10] != 0){
					yValX = sample[8];
					yValY = sample[9];
					yValZ = sample[10];
					cont_accel=0;
					}}
				else{cont_accel+=1;}		
		});
		socket.on('movimiento', function (data) {
			if(data.toString() == '1'){
				document.getElementById("mov3").classList.add('dar_sombra_iconos'); 
				if(document.getElementById("mov1").classList[0]=='dar_sombra_iconos'){
					document.getElementById("mov1").classList.remove('dar_sombra_iconos');
				}
				if(document.getElementById("mov2").classList[0]=='dar_sombra_iconos'){
					document.getElementById("mov2").classList.remove('dar_sombra_iconos');
				}
				if(document.getElementById("mov4").classList[0]=='dar_sombra_iconos'){
					document.getElementById("mov4").classList.remove('dar_sombra_iconos');
				}
			}
			else if(data.toString() == '2'){
				document.getElementById("mov4").classList.add('dar_sombra_iconos'); 
				if(document.getElementById("mov3").classList[0]=='dar_sombra_iconos'){
					document.getElementById("mov3").classList.remove('dar_sombra_iconos');
				}
				if(document.getElementById("mov1").classList[0]=='dar_sombra_iconos'){
					document.getElementById("mov1").classList.remove('dar_sombra_iconos');
				}
				if(document.getElementById("mov2").classList[0]=='dar_sombra_iconos'){
					document.getElementById("mov2").classList.remove('dar_sombra_iconos');
				}
			}
			else if(data.toString() == '3'){
				document.getElementById("mov2").classList.add('dar_sombra_iconos'); 
				if(document.getElementById("mov3").classList[0]=='dar_sombra_iconos'){
					document.getElementById("mov3").classList.remove('dar_sombra_iconos');
				}
				if(document.getElementById("mov1").classList[0]=='dar_sombra_iconos'){
					document.getElementById("mov1").classList.remove('dar_sombra_iconos');
				}
				if(document.getElementById("mov4").classList[0]=='dar_sombra_iconos'){
					document.getElementById("mov4").classList.remove('dar_sombra_iconos');
				}
			}
			else if(data.toString() == '4'){
				document.getElementById("mov1").classList.add('dar_sombra_iconos');
				if(document.getElementById("mov2").classList[0]=='dar_sombra_iconos'){
					document.getElementById("mov2").classList.remove('dar_sombra_iconos');
				}
				if(document.getElementById("mov4").classList[0]=='dar_sombra_iconos'){
					document.getElementById("mov4").classList.remove('dar_sombra_iconos');
				}
				if(document.getElementById("mov3").classList[0]=='dar_sombra_iconos'){
					document.getElementById("mov3").classList.remove('dar_sombra_iconos');
				}
			}
			else {
				if(document.getElementById("mov3").classList[0]=='dar_sombra_iconos'){
					document.getElementById("mov3").classList.remove('dar_sombra_iconos');
				}
				if(document.getElementById("mov1").classList[0]=='dar_sombra_iconos'){
					document.getElementById("mov1").classList.remove('dar_sombra_iconos');
				}
				if(document.getElementById("mov2").classList[0]=='dar_sombra_iconos'){
					document.getElementById("mov2").classList.remove('dar_sombra_iconos');
				}
				if(document.getElementById("mov4").classList[0]=='dar_sombra_iconos'){
					document.getElementById("mov4").classList.remove('dar_sombra_iconos');
				}
			}
		});

	}
	else { 
			alert("Debe haber al menos un canal marcado"); 
			document.getElementById(ultimo.toString()).checked = true;
			func();
	}


}

