/* Datos del web socket */	

	var grabacion = new Array();
	var grabacion2 = new Array();
	var grabacionAccel = new Array();
	var tiempo_sis;

	var chansettmode1 = "x1060110X";
	var chansettmode2 = "x2060110X";
	var chansettmode3 = "x3060110X";
	var chansettmode4 = "x4060110X";
	var chansettmode5 = "x5060110X";
	var chansettmode6 = "x6060110X";
	var chansettmode7 = "x7060110X";
	var chansettmode8 = "x8060110X";

	var si = [];
	var no = [];
	var ultimo = 0;


	var grabando = 0;
	var muestrasRec = 1;

function func(){
	document.getElementById("detener").disabled=true;
	
	while (document.getElementById("graficas").firstChild) {
    		document.getElementById("graficas").removeChild(document.getElementById("graficas").firstChild);
	}

	while (document.getElementById("graficas2").firstChild) {
    		document.getElementById("graficas2").removeChild(document.getElementById("graficas2").firstChild);
	}
	
		var nombresCanal = new Array()
		var todos_canales=document.getElementsByName("canal");
		var num_canales=0;
		for (var i = 0; i < todos_canales.length; i++)	{
			if(todos_canales[i].checked){
				num_canales++;
				nombresCanal.push(String(i+1));
				no.push(i+1);
			}
			else{
				si.push(i+1);
			}
		}
	
		if(no.length == 1){
			ultimo = no[0];
		}
		si = [];
		no = [];

		if(num_canales != 0){

	function Timeline(hechos, nombreCanal) {

		
		this.data = new TimeSeries();
		//this.chart = new SmoothieChart({maxValue:20000,minValue:17000});
		this.chart = new SmoothieChart();

		var num = 0;
		for (var i = 0; i < todos_canales.length; i++)	{
			if(todos_canales[i].checked)
				num++;
		}

		if(num==1){
		var parrafo = document.createElement("p");
		var contenidoParrafo = document.createTextNode("Canal "+nombreCanal);
		parrafo.appendChild(contenidoParrafo);
		parrafo.style.marginTop="70px";
		parrafo.style.marginBottom="-50px";
		parrafo.style.position="relative";

		this.chart.addTimeSeries(this.data, { strokeStyle: 'rgba(0, 255, 0, 1)', fillStyle: 'rgba(0, 255, 0, 0.2)', lineWidth: 1 });
		var lienzo = document.createElement("canvas");
		lienzo.setAttribute("width", "1200");
		lienzo.setAttribute("height", "300");
		lienzo.style.marginTop="70px";
		lienzo.style.marginBottom="-50px";
		lienzo.style.position="relative";
		lienzo.style.zIndex="1";
		this.chart.streamTo(lienzo, 500);
		document.getElementById("graficas").appendChild(parrafo);
		document.getElementById("graficas").appendChild(lienzo);

		}
		

		else if(num==2){
		var parrafo = document.createElement("p");
		var contenidoParrafo = document.createTextNode("Canal "+nombreCanal);
		parrafo.appendChild(contenidoParrafo);
		parrafo.style.marginTop="60px";
		parrafo.style.marginBottom="-70px";
		parrafo.style.position="relative";

		this.chart.addTimeSeries(this.data, { strokeStyle: 'rgba(0, 255, 0, 1)', fillStyle: 'rgba(0, 255, 0, 0.2)', lineWidth: 1 });
		var lienzo = document.createElement("canvas");
		lienzo.setAttribute("width", "1200");
		lienzo.setAttribute("height", "250");
		lienzo.style.marginTop="-40px";
		lienzo.style.marginBottom="70px";
		lienzo.style.position="relative";
		lienzo.style.zIndex="-1";
		this.chart.streamTo(lienzo, 500);
		document.getElementById("graficas").appendChild(parrafo);
		document.getElementById("graficas").appendChild(lienzo);

		}
		

		else if(num==3){
		var parrafo = document.createElement("p");
		var contenidoParrafo = document.createTextNode("Canal "+nombreCanal);
		parrafo.appendChild(contenidoParrafo);
		parrafo.style.marginTop="60px";
		parrafo.style.marginBottom="-70px";
		parrafo.style.position="relative";

		this.chart.addTimeSeries(this.data, { strokeStyle: 'rgba(0, 255, 0, 1)', fillStyle: 'rgba(0, 255, 0, 0.2)', lineWidth: 1 });
		var lienzo = document.createElement("canvas");
		lienzo.setAttribute("width", "1200");
		lienzo.setAttribute("height", "200");
		lienzo.style.marginTop="-40px";
		lienzo.style.marginBottom="70px";
		lienzo.style.position="relative";
		lienzo.style.zIndex="-1";
		this.chart.streamTo(lienzo, 500);
		document.getElementById("graficas").appendChild(parrafo);
		document.getElementById("graficas").appendChild(lienzo);

		}
		

		else if(num==4){
		var parrafo = document.createElement("p");
		var contenidoParrafo = document.createTextNode("Canal "+nombreCanal);
		parrafo.appendChild(contenidoParrafo);
		parrafo.style.marginTop="60px";
		parrafo.style.marginBottom="-70px";
		parrafo.style.position="relative";

		this.chart.addTimeSeries(this.data, { strokeStyle: 'rgba(0, 255, 0, 1)', fillStyle: 'rgba(0, 255, 0, 0.2)', lineWidth: 1 });
		var lienzo = document.createElement("canvas");
		lienzo.setAttribute("width", "1200");
		lienzo.setAttribute("height", "170");
		lienzo.style.marginTop="-40px";
		lienzo.style.marginBottom="70px";
		lienzo.style.position="relative";
		lienzo.style.zIndex="-1";
		this.chart.streamTo(lienzo, 500);
		document.getElementById("graficas").appendChild(parrafo);
		document.getElementById("graficas").appendChild(lienzo);

		}
		

		else if(num==5){
		//Si hechos==4, cambio de columna
		var parrafo = document.createElement("p");
		var contenidoParrafo = document.createTextNode("Canal "+nombreCanal);
		parrafo.appendChild(contenidoParrafo);
		parrafo.style.marginTop="60px";
		parrafo.style.marginBottom="-70px";
		parrafo.style.position="relative";

		
		this.chart.addTimeSeries(this.data, { strokeStyle: 'rgba(0, 255, 0, 1)', fillStyle: 'rgba(0, 255, 0, 0.2)', lineWidth: 1 });

		var lienzo = document.createElement("canvas");
		lienzo.setAttribute("width", "600");
		lienzo.setAttribute("height", "200");
		lienzo.style.marginTop="-30px";
		lienzo.style.marginBottom="70px";
		lienzo.style.position="relative";
		lienzo.style.zIndex="-1";
		this.chart.streamTo(lienzo, 500);
		if(hechos<3){
			document.getElementById("graficas").appendChild(parrafo);
			document.getElementById("graficas").appendChild(lienzo);
		}
		else {
			lienzo.style.marginLeft="20px";
			parrafo.style.marginLeft="20px";
			document.getElementById("graficas2").appendChild(parrafo);
			document.getElementById("graficas2").appendChild(lienzo);
		}

		}


		else if(num==6){
		//Si hechos==4, cambio de columna
		var parrafo = document.createElement("p");
		var contenidoParrafo = document.createTextNode("Canal "+nombreCanal);
		parrafo.appendChild(contenidoParrafo);
		parrafo.style.marginTop="60px";
		parrafo.style.marginBottom="-70px";
		parrafo.style.position="relative";

		
		this.chart.addTimeSeries(this.data, { strokeStyle: 'rgba(0, 255, 0, 1)', fillStyle: 'rgba(0, 255, 0, 0.2)', lineWidth: 1 });

		var lienzo = document.createElement("canvas");
		lienzo.setAttribute("width", "600");
		lienzo.setAttribute("height", "200");
		lienzo.style.marginTop="-30px";
		lienzo.style.marginBottom="70px";
		lienzo.style.position="relative";
		lienzo.style.zIndex="-1";
		this.chart.streamTo(lienzo, 500);
		if(hechos<3){
			document.getElementById("graficas").appendChild(parrafo);
			document.getElementById("graficas").appendChild(lienzo);
		}
		else {
			lienzo.style.marginLeft="20px";
			parrafo.style.marginLeft="20px";
			document.getElementById("graficas2").appendChild(parrafo);
			document.getElementById("graficas2").appendChild(lienzo);
		}

		}

		else if(num==7){
		//Si hechos==4, cambio de columna
		var parrafo = document.createElement("p");
		var contenidoParrafo = document.createTextNode("Canal "+nombreCanal);
		parrafo.appendChild(contenidoParrafo);
		parrafo.style.marginTop="60px";
		parrafo.style.marginBottom="-70px";
		parrafo.style.position="relative";

		
		this.chart.addTimeSeries(this.data, { strokeStyle: 'rgba(0, 255, 0, 1)', fillStyle: 'rgba(0, 255, 0, 0.2)', lineWidth: 1 });

		var lienzo = document.createElement("canvas");
		lienzo.setAttribute("width", "600");
		lienzo.setAttribute("height", "150");
		lienzo.style.marginTop="-30px";
		lienzo.style.marginBottom="70px";
		lienzo.style.position="relative";
		lienzo.style.zIndex="-1";
		this.chart.streamTo(lienzo, 500);
		if(hechos<4){
			document.getElementById("graficas").appendChild(parrafo);
			document.getElementById("graficas").appendChild(lienzo);
		}
		else {
			lienzo.style.marginLeft="20px";
			parrafo.style.marginLeft="20px";
			document.getElementById("graficas2").appendChild(parrafo);
			document.getElementById("graficas2").appendChild(lienzo);
		}

		}
		

		else if(num==8){
		//Si hechos==4, cambio de columna
		var parrafo = document.createElement("p");
		var contenidoParrafo = document.createTextNode("Canal "+nombreCanal);
		parrafo.appendChild(contenidoParrafo);
		parrafo.style.marginTop="60px";
		parrafo.style.marginBottom="-75px";
		parrafo.style.position="relative";

		
		this.chart.addTimeSeries(this.data, { strokeStyle: 'rgba(0, 255, 0, 1)', fillStyle: 'rgba(0, 255, 0, 0.2)', lineWidth: 1 });

		var lienzo = document.createElement("canvas");
		lienzo.setAttribute("width", "600");
		lienzo.setAttribute("height", "150");
		lienzo.style.marginTop="-30px";
		lienzo.style.marginBottom="70px";
		lienzo.style.position="relative";
		lienzo.style.zIndex="-1";
		this.chart.streamTo(lienzo, 500);
		if(hechos<4){
			document.getElementById("graficas").appendChild(parrafo);
			document.getElementById("graficas").appendChild(lienzo);
		}
		else {
			lienzo.style.marginLeft="20px";
			parrafo.style.marginLeft="20px";
			document.getElementById("graficas2").appendChild(parrafo);
			document.getElementById("graficas2").appendChild(lienzo);
		}

		}
	}


	Timeline.prototype.sample = function (s) {
		this.data.append(new Date().getTime(), s);
	}

	var nombresCanal = new Array()
	var todos_canales=document.getElementsByName("canal");
	var num_canales=0;
	for (var i = 0; i < todos_canales.length; i++)	{
		if(todos_canales[i].checked){
			num_canales++;
			nombresCanal.push(String(i+1));
		}
	}
	var waves = [];
	for (var i = 0; i < num_canales; i++) { //Numero de canales
		waves.push(new Timeline(i, nombresCanal[i]));
	}

	var j=0; var cont_accel = 0; var cont_accel2 = 0; var empezar = 0; var muestra=0; var ahora = 0; 

	var socket = io.connect('http://127.0.0.1');
		socket.on('openbci', function (sample) { 
				j=0;
				for(var i=0; i<8; i++){
					if(todos_canales[i].checked){
						var dato = sample[i];
						waves[j].sample(dato);
						j+=1;
						
						if(grabando == 1 ){
							if(cont_accel == 0) empezar = 1;
							if(empezar == 1){
								grabacion.push(dato);
							}
						}
					}
				}
				if(cont_accel > 8 && cont_accel < 10){
					if(sample[8] != 0 && sample[9] != 0 && sample[10] != 0){
						yValX = sample[8];
						yValY = sample[9];
						yValZ = sample[10];
						cont_accel=0;
						
					}
				}
				else{cont_accel+=1;}	
				if(empezar == 1){
					if(cont_accel2 > 8 && cont_accel2 < 10){
						if(yValX != 0 && yValY != 0 && yValZ != 0){
							cont_accel2=0;
							grabacionAccel[grabacionAccel.length] = yValX;
							grabacionAccel[grabacionAccel.length] = yValY;
							grabacionAccel[grabacionAccel.length] = yValZ;
						}
					}
					else{cont_accel2+=1;}}
				
				string = '';
				j=0;
				for(var i=0; i<8; i++){
					if(todos_canales[i].checked){
						var dato = sample[i];
						if(dato.toFixed(6).split('.')[1][5] == 0) {
							if(i==0){string = dato.toFixed(5);}
							else{string = string +","+ dato.toFixed(5);}
						}
						else if(dato.toFixed(7).split('.')[1][6] == 0 && i==7) {
							string = string +","+ dato.toFixed(6);
						}
						else{
							if(i==0){string = dato.toFixed(6);}
							else if(i==7){string = string +"," + dato.toFixed(7);}
							else{string = string +","+ dato.toFixed(6);}
						}
					}
				}
				string = string +","+ sample[8]+","+ sample[9]+","+ sample[10]+",";
				if(grabando == 1 ){
					if(sample[8] != 0.0 && sample[9] != 0.0 && sample[10] != 0.0) ahora = 1;
					if(ahora == 1)
						grabacion2.push(string);
				}
				/*if(grabando == 1 ){
				muestrasRec += 1;
				if(muestrasRec == 25){muestrasRec = 0; display();}
				}*/


		});
		socket.on('csmode', function (data) {
			var canal = data.substring(1)[0];
			if(canal == '1')
				chansettmode1 = data; 
			else if(canal == '2')
				chansettmode2 = data;
			else if(canal == '3')
				chansettmode3 = data;
			else if(canal == '4')
				chansettmode4 = data;
			else if(canal == '5')
				chansettmode5 = data;
			else if(canal == '6')
				chansettmode6 = data;
			else if(canal == '7')
				chansettmode7 = data;
			else if(canal == '8')
				chansettmode8 = data;
		});

	}
	else { 
			alert("Debe haber al menos un canal marcado"); 
			document.getElementById(ultimo.toString()).checked = true;
			func();
	}

}




//***************************************************************************************************//
/* CONTADOR + GRABADOR */
	
	var milisec=0
	var seconds=0
	var det=0
	var tiempo_grabacion = 0

	function display(){
		
		if (milisec>=9){
			milisec=0
			seconds+=1
		}
		else{
			milisec+=1
		}

		if(seconds != document.getElementById("segundos").value && det==0){
			document.d.d2.value=seconds+"."+milisec
			setTimeout("display()",100)
		}
		else {
			if(det==0){
				document.d.d2.value=seconds+"."+milisec
				tiempo_grabacion = seconds+"."+milisec
				det=1
			}
			tiempo_grabacion = seconds+"."+milisec
			document.getElementById("grabar").disabled=false;
			document.getElementById("detener").disabled=true;
			grabando = 0;
			empezar = 0;
			
			//*******Acaba la grabacion*********
			previsualizacion();


		}
	}

	function display2(){

		document.d.d2.value='0'
		grabacion = new Array()
		grabacionAccel = new Array()
		grabando = 1
		var f=new Date();
		var mes = f.getMonth() +1;
		//if(mes == 1 || mes == 2 || mes == 3 || mes == 4 || mes == 5 || mes == 6 || mes == 7 || mes == 8 || mes == 9) mes = "0"+mes;
		var dia = f.getDate();
		//if(dia == 1 || dia == 2 || dia == 3 || dia == 4 || dia == 5 || dia == 6 || dia == 7 || dia == 8 || dia == 9) dia = "0"+dia;
		var h = f.getHours();
		//if(h == 1 || h == 2 || h == 3 || h == 4 || h == 5 || h == 6 || h == 7 || h == 8 || h == 9) h = "0"+h;
		var m = f.getMinutes();
		//if(m == 1 || m== 2 || m == 3 || m == 4 || m == 5 || m == 6 || m== 7 || m == 8 || m == 9) m = "0"+m;
		var s = f.getSeconds();
		//if(s == 1 || s== 2 || s == 3 || s == 4 || s == 5 || s == 6 || s== 7 || s == 8 || s == 9) s = "0"+s;
		tiempo_sis = dia + "/" + mes + "/" + f.getFullYear()+ " " +h+":"+m+":"+s;

		document.getElementById("grabar").disabled=true;
		document.getElementById("detener").disabled=false;

		if ( document.getElementById("aqui").hasChildNodes() ){
		while ( document.getElementById("aqui").childNodes.length >= 1 ){
			document.getElementById("aqui").removeChild(document.getElementById("aqui").firstChild);
		}
		}
		
		milisec=0
		seconds=0
		det=0
		if (milisec>=9){
			milisec=0
			seconds+=1
		}
		else
			milisec+=1
		document.d.d2.value=seconds+"."+milisec
		setTimeout("display()",100)
	}

	function detener(){
		document.getElementById("grabar").disabled=false;
		document.getElementById("detener").disabled=true;

		det=1
		grabando = 0


	}



