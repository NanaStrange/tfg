var dgram = require('dgram');
var events = require('events');
var fs = require('fs');
var http = require('http');
var io = require('socket.io');

var UDP_HOST = '127.0.0.1',
    UDP_PORT = 8888,
    SERVER_HOST = '127.0.0.1',
    SERVER_PORT = 8880,
    HTDOCS_PATH = '/htdocs';

/*SOCKET PARA LOS DATOS DE LOS CANALES*/
function UDPClient(port, host) {
  this.port = port;
  this.host = host;
  this.data = [];
  this.events = new events.EventEmitter();
  this.connection = dgram.createSocket('udp4');
  this.connection.on('listening', this.onListening.bind(this));
  this.connection.on('message', this.onMessage.bind(this));
  this.connection.bind(this.port, this.host);
};


UDPClient.prototype.onListening = function() {
  console.log('Esperando conexión de cliente...');
};

UDPClient.prototype.onMessage = function(msg) {
  this.events.emit('sample', JSON.parse(msg.toString()));
};



function OpenBCIServer(host, port, htdocs) {
  this.host = host;
  this.port = port;
  this.servidor = http.createServer(this.onRequest.bind(this));
  this.socket = io.listen(this.servidor, { log: false });
  this.htdocs = htdocs;

  this.socket.on('connection', this.onSocketConnect.bind(this));
  this.servidor.listen(this.port, this.host)
};

OpenBCIServer.prototype.onRequest = function(req, res) {
  this.serveStatic(req, res);
};

OpenBCIServer.prototype.serveStatic = function(req, res) {
  var file = req.url;
  if (file == '/') {
    file = '/index.html';
  }
  fs.readFile(__dirname + this.htdocs + file,
      function(err, data) {
        if (err) {
          console.log(err);
          res.writeHead(500);
          return res.end('Error loading index.html');
        }

        res.writeHead(200);
        res.end(data);
      });
};

OpenBCIServer.prototype.onSocketConnect = function(socket) {
  console.log('Cliente conectado');
};

var cliente = new UDPClient(UDP_PORT, UDP_HOST);
var servidor = new OpenBCIServer(SERVER_HOST, SERVER_PORT, HTDOCS_PATH);

cliente.events.on('sample', function(data) {
  servidor.socket.sockets.emit('openbci', data);
});

/*SOCKET PARA EL CHANNEL SETTINGS MODE*/
function UDPClient2(port, host) {
  this.port = port;
  this.host = host;
  this.data = [];
  this.events = new events.EventEmitter();
  this.connection = dgram.createSocket('udp4');
  this.connection.on('listening', this.onListening.bind(this));
  this.connection.on('message', this.onMessage.bind(this));
  this.connection.bind(this.port, this.host);
};


UDPClient2.prototype.onListening = function() {
  console.log('Esperando channel settings mode...');
};

UDPClient2.prototype.onMessage = function(msg) {
  console.log('Channel settings mode cambiado');
  this.events.emit('csm', msg.toString());
};

var cliente_csm = new UDPClient2(8181, '127.0.0.1');

cliente_csm.events.on('csm', function(x) {
  servidor.socket.sockets.emit('csmode', x);
});


/*SOCKET PARA EL RECONOCIMIENTO DE MOVIMIENTOS*/
function UDPClient3(port, host) {
  this.port = port;
  this.host = host;
  this.data = [];
  this.events = new events.EventEmitter();
  this.connection = dgram.createSocket('udp4');
  this.connection.on('message', this.onMessage.bind(this));
  this.connection.bind(this.port, this.host);
};

UDPClient3.prototype.onMessage = function(msg) {
  this.events.emit('mov', msg.toString());
};

var cliente_mov = new UDPClient3(8282, '127.0.0.1');

cliente_mov.events.on('mov', function(m) {
  servidor.socket.sockets.emit('movimiento', m);
});

