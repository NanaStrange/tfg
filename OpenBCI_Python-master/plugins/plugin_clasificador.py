import plugin_interface as plugintypes
import socket

class PluginClasificador(plugintypes.IPluginExtended):

	def __init__(self): #inicializacion
		print ""
  		self.prueba = "1"          
		self.contador = 0

	def activate(self): 
		print "Plugin clasificador en marcha."
		
	def deactivate(self):
		print "Plugin clasificador off."

	def __call__(self, sample): #al ejecutarlo
         print "Plugin ejecutandose. Hurra!"
         server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
         #Se pasaria el sample al clasificador y este devolveria un numero que se envia al socket y en forma de orden al robot
         if(self.contador<1000):
             self.prueba = "1"
         elif(self.contador<2000):
             self.prueba = "5"
         elif(self.contador<3000):
             self.prueba = "8"
        
         self.contador = self.contador + 1;
         if(self.contador == 3000):
             self.contador = 0
         server.sendto(self.prueba, ('localhost', 8282))
